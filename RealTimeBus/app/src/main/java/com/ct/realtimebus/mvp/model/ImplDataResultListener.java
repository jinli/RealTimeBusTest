package com.ct.realtimebus.mvp.model;

import android.content.Context;
import android.widget.Toast;

import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;

/**
 * Created by zhangjinli on 16/9/7.
 */
public class ImplDataResultListener implements OnDataResultListener{
    private Context mContext;

    public ImplDataResultListener(Context context) {
        this.mContext = context;
    }

    @Override
    public void onSuccess(String resultData) {

    }

    @Override
    public void onFailure(String msg, Exception e) {
        Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();
    }
}
