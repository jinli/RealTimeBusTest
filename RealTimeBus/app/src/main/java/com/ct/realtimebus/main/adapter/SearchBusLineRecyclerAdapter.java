package com.ct.realtimebus.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.BusHelper;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.lineinfo.activity.LineInfoActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by zhangjinli on 16/7/29.
 */
public class SearchBusLineRecyclerAdapter extends RecyclerView.Adapter<SearchBusLineRecyclerAdapter.ItemViewHolder> {

    private Context mContext;

    private JSONArray busLines;
    private LayoutInflater mLayoutInflater;
    private JSONObject searchStop;
    private HashMap<String, JSONArray> linesMap;
    private double rates[] = {0.25, 0.5, 0.75};
    private String[] strRates = {"空车", "舒适", "满座", "拥挤"};
    private int[] dawRates = {
            R.drawable.bus_inside_empty,
            R.drawable.bus_inside_comfortable,
            R.drawable.bus_inside_full,
            R.drawable.bus_inside_crowded};

    public SearchBusLineRecyclerAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public SearchBusLineRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewHolder itemViewHolder = new ItemViewHolder(mLayoutInflater.inflate(R.layout.item_main_bus_list, parent, false));
        return itemViewHolder;
    }


    @Override
    public void onBindViewHolder(SearchBusLineRecyclerAdapter.ItemViewHolder holder, int position) {

        try {
            final JSONObject busLine = busLines.getJSONObject(position);
            holder.tvBusNo.setText(busLine.getString("line").replace("路", ""));
            holder.tvBusArrivalTime.setText("--");
            holder.tvBusStationName.setText(busLine.getString("terminal"));
            holder.tvBusServiceTime.setText(busLine.getString("startTime") + "-" + busLine.getString("endTime"));
            if (busLine.has("buses")) {
                final JSONArray buses = busLine.getJSONArray("buses");
                if (buses != null && buses.length() > 0) {
                    JSONArray bs = new JSONArray();
                    for(int i = 0; i < buses.length(); i ++){
                        if(buses.getJSONObject(i).getInt("lineId") == busLine.getInt("id")){
                            bs.put(buses.getJSONObject(i));
                        }
                    }
//                    JSONObject bus = BusHelper.getNearBus(bs, searchStop);
//                    if (bus != null && bus.getInt("lineId") == busLine.getInt("id")) {
//                        if(bus.getInt("eta") / 60 > 2){
//                            holder.tvBusArrivalTime.setText(bus.getInt("eta") / 60 + "min");
//                        }else{
//                            holder.tvBusArrivalTime.setText(ResourcesHelper.getString(mContext, R.string.text_is_arrviing));
//                        }
//                        double rate = 0;
//                        if(bus.has("rate")){
//                            rate = bus.getDouble("rate");
//                        }
//                        if (rate < rates[0]) {
//                            holder.tvBusInsideStatus.setText(strRates[0]);
//                            holder.imgBusInsideStatus.setImageResource(dawRates[0]);
//                        } else if (rate > rates[0] && rate < rates[1]) {
//                            holder.tvBusInsideStatus.setText(strRates[1]);
//                            holder.imgBusInsideStatus.setImageResource(dawRates[1]);
//                        } else if (rate > rates[1] && rate < rates[2]) {
//                            holder.tvBusInsideStatus.setText(strRates[2]);
//                            holder.imgBusInsideStatus.setImageResource(dawRates[2]);
//                        } else if (rate > rates[2]) {
//                            holder.tvBusInsideStatus.setText(strRates[3]);
//                            holder.imgBusInsideStatus.setImageResource(dawRates[3]);
//                        } else {
//                            holder.tvBusInsideStatus.setText("--");
//                            holder.imgBusInsideStatus.setImageResource(dawRates[0]);
//                        }
//                    } else {
//                        holder.tvBusInsideStatus.setText("--");
//                        holder.imgBusInsideStatus.setImageResource(dawRates[0]);
//                    }

                } else {
                    holder.tvBusInsideStatus.setText("--");
                    holder.imgBusInsideStatus.setImageResource(dawRates[0]);
                }
            }else{
                holder.tvBusInsideStatus.setText("--");
                holder.imgBusInsideStatus.setImageResource(dawRates[0]);
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, LineInfoActivity.class);
                    try {
                        intent.putExtra(Constants.IntentKey.LINE_ID, linesMap.get(busLine.getString("line")).toString());
                        intent.putExtra(Constants.IntentKey.STOP_ID, searchStop.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mContext.startActivity(intent);
                }
            });

            //==============测试代码======
            DecimalFormat df = new DecimalFormat("0.00");
            Log.v("======", new Random().nextInt(30) + "====");
            if(!busLine.has("rate")){
                busLine.put("rate",df.format(Math.random()));
            }
            double rate = busLine.getDouble("rate");
            if(rate < rates[0]){
                holder.tvBusInsideStatus.setText(strRates[0]);
                holder.imgBusInsideStatus.setImageResource(dawRates[0]);
            }else if(rate > rates[0] && rate < rates[1]){
                holder.tvBusInsideStatus.setText(strRates[1]);
                holder.imgBusInsideStatus.setImageResource(dawRates[1]);
            }else if(rate > rates[1] && rate < rates[2]){
                holder.tvBusInsideStatus.setText(strRates[2]);
                holder.imgBusInsideStatus.setImageResource(dawRates[2]);
            }else if(rate > rates[2]){
                holder.tvBusInsideStatus.setText(strRates[3]);
                holder.imgBusInsideStatus.setImageResource(dawRates[3]);
            }

            if(!busLine.has("at")){
                busLine.put("at",new Random().nextInt(30));
            }
            int at = busLine.getInt("at");
            if(at < 6){
                holder.tvBusArrivalTime.setText("即将到达");
            }else{
                holder.tvBusArrivalTime.setText(at+"min");
            }
            //==============测试代码======
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgBusInsideStatus;
        private TextView tvBusInsideStatus;
        private TextView tvBusNo;
        private TextView tvBusStationName;
        private TextView tvBusServiceTime;
        private TextView tvBusArrivalTime;
        private View view;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            imgBusInsideStatus = (ImageView) itemView.findViewById(R.id.img_bus_inside_status);
            tvBusInsideStatus = (TextView) itemView.findViewById(R.id.tv_bus_inside_status);
            tvBusNo = (TextView) itemView.findViewById(R.id.tv_bus_no);
            tvBusStationName = (TextView) itemView.findViewById(R.id.tv_bus_station_name);
            tvBusServiceTime = (TextView) itemView.findViewById(R.id.tv_bus_service_time);
            tvBusArrivalTime = (TextView) itemView.findViewById(R.id.tv_bus_arrival_time);
        }
    }


    public void setBusLines(JSONArray busLines) {
        this.busLines = busLines;
    }

    public void setSearchStop(JSONObject searchStop) {
        this.searchStop = searchStop;
    }

    public void setLinesMap(HashMap<String, JSONArray> linesMap) {
        this.linesMap = linesMap;
    }

    @Override
    public int getItemCount() {
        return busLines != null ? busLines.length() : 0;
    }

}
