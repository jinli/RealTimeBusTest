package com.ct.realtimebus.main;

import android.content.Context;

import com.ct.realtimebus.bean.HistoryBean;
import com.ct.realtimebus.service.HistoryService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by zhangjinli on 16/8/30.
 */
public class LruHistory {
    private Context context;

    private int maxSize;
    public static final int SUBSCRIBE = 0;
    public static final int READ = 1;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
    private LinkedHashMap<Integer, HistoryBean> cache = new LinkedHashMap<>(maxSize);

    public LruHistory(int maxSize, Context context) {
        this.maxSize = maxSize;
        this.context = context;
    }

    public void add(HistoryBean historyBean) {
        /**如果缓存中没有数据**/
        if (!cache.containsKey(historyBean.getLineId())) {
            /**判断如果小于最大容量 直接存入头部**/
            if (cache.size() < maxSize) {
                cache.put(historyBean.getLineId(), historyBean);
            }else{
                remove();
                cache.put(historyBean.getLineId(), historyBean);
            }
        }else{/**如果缓存中有数据**/
            move2Head(historyBean);
        }
        updateDB();
    }

    public void move2Head(HistoryBean historyBean) {
        HistoryBean cacheHistoryBean = cache.get(historyBean.getLineId());
        cache.remove(historyBean.getLineId());
        if(historyBean.getSubscribeOrRead() == SUBSCRIBE){
            if(historyBean.getSubscribeOrRead() == cacheHistoryBean.getSubscribeOrRead()){
                historyBean.setSorTimes(historyBean.getSorTimes() + 1);
            }else{
                historyBean.setSorTimes(1);
            }
        }else if(historyBean.getSubscribeOrRead() == READ){
            if(historyBean.getSubscribeOrRead() == cacheHistoryBean.getSubscribeOrRead()){
                historyBean.setSorTimes(historyBean.getSorTimes() + 1);
            }else{
                /** 订阅过的信息不改变为浏览 **/
                historyBean.setSubscribeOrRead(SUBSCRIBE);
                historyBean.setSorTimes(1);
            }
        }
        cache.put(cacheHistoryBean.getLineId(),historyBean);
    }

    public void remove(){
        /**将已经进行过LRU排序的集合使用FIFO算法处理 移除时间最久远的一条**/
        Date date = new Date();
        int lineId = 0;
        for(Integer key : cache.keySet()){
            HistoryBean historyBean = cache.get(key);
            try {
                Date createDate = sdf.parse(historyBean.getCreateDate());
                if(date.compareTo(createDate) > 0){
                    date = createDate;
                    lineId = historyBean.getLineId();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        cache.remove(lineId);
    }

    public void updateDB(){
        HistoryService.startUpdateHistory(context);
    }

    /**
     * 以数据库中的数据进行初始化
     */
    public void initNodes(ArrayList<HistoryBean> historyBeans) {
        if (historyBeans != null && historyBeans.size() > 0) {
            for (HistoryBean historyBean : historyBeans) {
                this.cache.put(historyBean.getLineId(), historyBean);
            }
        }
    }

    public LinkedHashMap<Integer, HistoryBean> getCache() {
        return cache;
    }
}


