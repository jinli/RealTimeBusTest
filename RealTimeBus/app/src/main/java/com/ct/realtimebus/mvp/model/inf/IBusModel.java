package com.ct.realtimebus.mvp.model.inf;

import android.content.Context;

import com.ct.realtimebus.mvp.model.ImplDataResultListener;

/**
 * Created by zhangjinli on 16/9/6.
 */
public interface IBusModel {
    /**
     * 根据id查询公交信息
     * @param id
     */
    void getBusInfoById(int id,ImplDataResultListener onDataResultListener);

    void setContext(Context context);
}
