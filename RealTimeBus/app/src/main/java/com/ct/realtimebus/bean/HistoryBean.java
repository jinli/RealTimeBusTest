package com.ct.realtimebus.bean;

import java.util.Date;

/**
 * 历史纪录数据实体
 * Created by zhangjinli on 16/8/29.
 */
public class HistoryBean {

    private int id;
    private int stopId;
    private String stopName;
    private int lineId;
    private String lineName;
    private String createDate;
    private String fromStopName;
    private String toStopName;
    private int subscribeOrRead;//是订阅还是浏览 0-订阅 1-浏览
    private int sorTimes;//订阅或者浏览次数

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getFromStopName() {
        return fromStopName;
    }

    public void setFromStopName(String fromStopName) {
        this.fromStopName = fromStopName;
    }

    public String getToStopName() {
        return toStopName;
    }

    public void setToStopName(String toStopName) {
        this.toStopName = toStopName;
    }

    public int getSubscribeOrRead() {
        return subscribeOrRead;
    }

    public void setSubscribeOrRead(int subscribeOrRead) {
        this.subscribeOrRead = subscribeOrRead;
    }

    public int getSorTimes() {
        return sorTimes;
    }

    public void setSorTimes(int sorTimes) {
        this.sorTimes = sorTimes;
    }
}
