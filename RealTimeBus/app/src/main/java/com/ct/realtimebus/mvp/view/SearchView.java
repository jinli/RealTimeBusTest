package com.ct.realtimebus.mvp.view;

import com.ct.realtimebus.bean.BeanStop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/19.
 */
public interface SearchView {

    /**
     * 检索数据后 更新检索结果列表
     * @param stops
     */
    void updateSearchStopList(ArrayList<BeanStop> stops);

    /**
     * 更新地图数据
     * @param stops
     */
    void updateMap(ArrayList<BeanStop> stops);

}
