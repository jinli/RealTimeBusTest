package com.ct.realtimebus.common;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangjinli on 16/8/2.
 */
public class VolleyHelper extends Application{

    public static final String RESULT = "result";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";

    public static final int TIME_OUT = 30000;

    private RequestQueue queue;


    public VolleyHelper(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public void createRequest(String url, int method, final HashMap<String, String> params, Response.Listener<String> successListener, Response.ErrorListener errorListener){
        String regex = "[\u4E00-\u9FA5]+";
        String strParams = "?";
        int i = 0;
        for(String key : params.keySet()){
            String param = params.get(key);
            strParams = strParams + key+"=";
            if(param.matches(regex)){
                try {
                    strParams = strParams + URLEncoder.encode(param, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }else{
                strParams = strParams + param;
            }
            i ++;
            if(i < params.size()){
                strParams = strParams + "&";
            }

        }

        StringRequest stringRequest = new StringRequest(method,url + strParams,successListener,errorListener){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> map = new HashMap<>();
                map.put("Charset", "utf-8");
                return super.getHeaders();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void start(){
        queue.start();
    }

    /**
     * 返回结果进行检查
     * @param r
     * @return
     */
    public static HashMap<String,String> checkResult(String r) throws JSONException{
        HashMap<String,String> resultMap = new HashMap<>();
        JSONObject result = new JSONObject(r);
        int status = result.getInt(STATUS);
        if(status != 0){
            resultMap.put(MESSAGE,result.getString(MESSAGE));
        }else{
            resultMap.put(RESULT,result.getString(RESULT));
        }
        return resultMap;
    }

    /**
     * 检查是否含有message如果有返回true
     * @param result
     * @return
     */
    public static boolean isResultError(HashMap<String,String> result){
        if(result.containsKey(MESSAGE)){
            return true;
        }else{
            return false;
        }
    }

}
