package com.ct.realtimebus.lineinfo.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.test.MyTestSingle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 自定义时间轴Item 作为RecyclerView的Item布局使用
 * Created by zhangjinli on 16/8/7.
 */
public class TimeLineItemView extends View {
    private Context mContext;
    private BeanStop stopDetail;
    private ArrayList<BeanBus> buses;
    private BeanStop currentStop;
    private int position;

    private int currentStopPosition;
    private int currentLineId;

    private boolean isStartStop = false;
    private boolean isTerminal = false;

    private double rates [] = {0.25,0.5,0.75};

    public TimeLineItemView(Context context) {
        super(context);
        mContext = context;
    }

    public TimeLineItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public TimeLineItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimeLineItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
            int width = getWidth();
            int height = getHeight();
            final int paddingleft = getPaddingLeft();
            final int paddingRight = getPaddingRight();
            final int paddingTop = getPaddingTop();
            final int paddingBottom = getPaddingBottom();

            final String stopName = stopDetail.getName();
            final double stopLat = stopDetail.getLatitude();
            final double stoplng = stopDetail.getLongitude();
            final int stopId = stopDetail.getId();

            //初始化画笔
            Paint paint = new Paint();
            paint.setAntiAlias(true);//设置抗锯齿

            float radius = getResources().getDimension(R.dimen.time_line_radius);//设置大圆半径

            //绘制一条线
            paint.setStrokeWidth(3f);//设置线的宽度
            //设置线的颜色
            if(position <= currentStopPosition){
                paint.setColor(ResourcesHelper.getColor(mContext,R.color.colorBusLineAfterStop));
            }else{
                paint.setColor(mContext.getResources().getColor(R.color.colorBusLineSoonArrivesStop));
            }
            if(isStartStop){
                canvas.drawLine(paddingleft + radius,height - paddingBottom, paddingleft + radius, height, paint);
            }else if(isTerminal){
                canvas.drawLine(paddingleft + radius,0, paddingleft + radius, height - paddingBottom - radius, paint);
            }else{
                canvas.drawLine(paddingleft + radius,0, paddingleft + radius, height, paint);
            }

            //设置圆的颜色
            if(isStartStop){
                paint.setColor(mContext.getResources().getColor(R.color.colorBusLineStartStationOval));//设置画笔的颜色
            }else if(isTerminal){
                paint.setColor(mContext.getResources().getColor(R.color.colorBusLineEndStationOval));//设置画笔的颜色
            }else{
                paint.setColor(mContext.getResources().getColor(R.color.colorBusLineSoonArrivesStop));//设置画笔的颜色
            }
            //绘制一个实心的圆
            paint.setStyle(Paint.Style.FILL);//设置画笔为实心
            canvas.drawCircle(paddingleft + radius, height - paddingBottom - radius, radius, paint);//绘制大圆

            //根据条件判断是否绘制小圆
            if(position < currentStopPosition && position > 0){
                paint.setColor(Color.WHITE);//设置小圆颜色
                canvas.drawCircle(paddingleft + radius, height - radius - paddingBottom, radius / 3 * 2, paint);//绘制小圆大小为大圆的2/3
            }

            if(position == currentStopPosition){
                Bitmap myLocation = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.route_my_location);
                canvas.drawBitmap(myLocation, paddingleft - (myLocation.getWidth() - radius * 2) / 2, height - paddingBottom - ((int) radius * 2) - (myLocation.getHeight() - (int) radius * 2) / 2, null);
                canvas.drawLine(paddingleft + radius,height - paddingBottom - ((int) radius * 2) - (myLocation.getHeight() - (int) radius * 2) / 2 + myLocation.getHeight(), paddingleft + radius, height, paint);
            }

            /**
             * 判断车辆的下一站是否是该站
             */
            LatLng stopLatlng = new LatLng(stopLat, stoplng);
        for(int i = 0; i < buses.size(); i ++){
            BeanBus bus = buses.get(i);
            double busLat = bus.getLatitude();
            double busLng = bus.getLongitude();
            if(stopId == bus.getStopId()){
                LatLng busLatlng = new LatLng(busLat,busLng);
                double rate = bus.getRate();
                int drawable = R.drawable.route_empty;
                if(rate < rates[0]){
                    drawable = R.drawable.route_empty;
                }else if(rate > rates[0] && rate < rates[1]){
                    drawable = R.drawable.route_comfortable;
                }else if (rate > rates[1] && rate < rates[2]){
                    drawable = R.drawable.route_full;
                }else if(rate > rates[2]){
                    drawable = R.drawable.route_crowded;
                }
                Bitmap busLocation = BitmapFactory.decodeResource(mContext.getResources(), drawable);
                double distance = bus.getDistance();
                MyTestSingle mts = MyTestSingle.getInstance();
                double totalDistance = mts.getTotleDistance();
                double d[] = {totalDistance,totalDistance/3*2,totalDistance/3};
//                    Log.v("+++++", stopId + "=========" + distance + "========" + totalDistance);
                int busPosition = 0;
                if(distance < d[0] && distance > d[1]){
                    busPosition = height/3;
                }else if(distance < d[1] && distance > d[2]){
                    busPosition = height/3*2;
                }else if(distance < d[2]){
                    busPosition = height - paddingBottom - ((int)radius * 2) - (busLocation.getHeight() - (int)radius*2)/2;
                }
                //绘制车辆位置图片
                canvas.drawBitmap(busLocation, paddingleft - (busLocation.getWidth() - radius * 2) / 2, busPosition, null);

                // TODO 绘制预计到达时间文字
//                    float stopNameTextSize = getResources().getDimension(R.dimen.text_size_time_line_stop_name);
//                    paint.setTextSize(stopNameTextSize);//设置文字大小
//                    paint.setColor(ResourcesHelper.getColor(mContext,R.color.colorTextBusListItemTime));
//                    canvas.drawText("3min", 0, height - paddingBottom, paint);
            }
        }

            //绘制文字
            float stopNameTextSize = getResources().getDimension(R.dimen.text_size_time_line_stop_name);
            paint.setTextSize(stopNameTextSize);//设置文字大小
            if(position < currentStopPosition){
                paint.setColor(mContext.getResources().getColor(R.color.colorTextTimelineShallowBlue));//设置画笔颜色
            }else if (position == currentStopPosition){
                paint.setColor(mContext.getResources().getColor(R.color.colorTextTimelineMiddleBlue));//设置画笔颜色
            }else if(position > currentStopPosition){
                paint.setColor(mContext.getResources().getColor(R.color.colorTextTimelineDeepBlue));//设置画笔颜色
            }

            canvas.drawText(stopName, paddingleft + radius * 2 + 10, height - paddingBottom, paint);
        super.onDraw(canvas);
    }

    public void setStopDetail(BeanStop stopDetail) {
        this.stopDetail = stopDetail;
    }

    public void setIsStartStop(boolean isStartStop) {
        this.isStartStop = isStartStop;
    }

    public void setIsTerminal(boolean isTerminal) {
        this.isTerminal = isTerminal;
    }

    public void setCurrentStop(BeanStop currentStop) {
        this.currentStop = currentStop;
    }

    public void setBuses(ArrayList<BeanBus> buses) {
        this.buses = buses;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setCurrentLineId(int currentLineId){
        this.currentLineId = currentLineId;
    }

    public void setCurrentStopPosition(int currentStopPosition) {
        this.currentStopPosition = currentStopPosition;
    }
}
