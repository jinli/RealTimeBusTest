package com.ct.realtimebus.common;

/**
 * Created by zhangjinli on 16/7/28.
 */
public class Constants {
    public static final String TTF_URI = "iconfont/fontawesome-webfont.ttf";

    public static final String TEST_CURRENT_CITY = "BEIJING";
    public static final String TEST_LENGTH = "5";

    public static final String PARAM_NAME_LNG = "lng";
    public static final String PARAM_NAME_LAT = "lat";
    public static final String PARAM_NAME_CITY = "city";
    public static final String PARAM_NAME_LENGTH = "length";
    public static final String PARAM_NAME_QUERY = "q";

    /**
     * Intent跨进程通信KEY
     */
    public class IntentKey {
        public static final String LINE_ID = "lineId";
        public static final String STOP_ID = "stopId";
        public static final String LINE_DETAIL = "lineDetail";
        public static final String STOP_DETAIL = "shopDetail";
        public static final String REMIND_STOP = "remindStop";
        public static final String REMIND_BUS = "remindBus";
        public static final String REMIND_LINE = "remindLine";
    }

    /**
     * SharedPreferences序列化文件Key
     */
    public static final String SHARED_REMIND = "shared_remind";//用户通知设置缓存KEY

    public class SharedRemindKey {
        public static final String GET_OFF_REMIND = "getOffRemind";
        public static final String COME_BUS_REMIND = "comeBusRemind";
        public static final String TERMINAL = "terminal";
        public static final String CURRENT_STOP = "currentStop";
        public static final String REMIND_LINE = "remindLine";
    }
}
