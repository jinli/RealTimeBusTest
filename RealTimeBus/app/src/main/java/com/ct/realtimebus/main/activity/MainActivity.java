package com.ct.realtimebus.main.activity;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.ct.realtimebus.R;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.PermissionHelper;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.widget.PermissionAllActivity;
import com.ct.realtimebus.main.fragment.HistoryFragment;
import com.ct.realtimebus.main.fragment.MainFragment;


/**
 * 首页 activity
 *
 * Create by zhangjinli 2016/07/28
 */
public class MainActivity extends PermissionAllActivity implements MainFragment.OnFragmentInteractionListener,
        OnRequestPermissionsResultCallback,RadioGroup.OnCheckedChangeListener,AMapLocationListener{

    private static final String TAG = "MainActivity";
    private long exitTime = 2000l;


    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private AMapLocationClient mLocationClient;//定位客户端
    private AMapLocationClientOption mLocationClientOption;//定位客户端参数设置
    private AMapLocationListener mLocationListener;//定位回调监听
    private SharedPreferences mSharedPreferences;
    private ImageButton btnRemind;

    private MainFragment mainFragment;
    private HistoryFragment historyFragment;

    private RadioGroup rgBottombar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreferences = getSharedPreferences(Constants.SHARED_REMIND,0);
        btnRemind = (ImageButton)findViewById(R.id.btn_reminder);

        rgBottombar = (RadioGroup)findViewById(R.id.bottom_bar);
        rgBottombar.setOnCheckedChangeListener(this);
        //初始化定位客户端
        mLocationClient = new AMapLocationClient(getApplicationContext());
        //设置定位客户端参数
        mLocationClientOption = new AMapLocationClientOption();
        mLocationClientOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//设置高精度定位模式
        mLocationClientOption.setNeedAddress(true);//设置返回地址信息
        /**App需求30s刷新一次，开发需要暂时关闭**/
        mLocationClientOption.setOnceLocation(true);//设置是否只定位一次
        if(mLocationClientOption.isOnceLocationLatest()){
            mLocationClientOption.setOnceLocationLatest(true);
        }
        mLocationClientOption.setWifiActiveScan(true);//设置是否强制刷新wifi
        mLocationClientOption.setMockEnable(false);//设置不允许模拟位置
        mLocationClientOption.setInterval(30000l);//设置定位时间间隔
        mLocationClient.setLocationOption(mLocationClientOption);
        initFragment();
        mLocationClient.setLocationListener(this);
        /**检查用户定位权限做后续处理**/
        if(PermissionHelper.checkPermission(this,PermissionHelper.PERMISSION_COARSE_LOCATION)){
            if(!mLocationClient.isStarted()){
                mLocationClient.startLocation();
            }
        }

    }

    public void locationSwitch(boolean on){
        if(on){
            mLocationClient.startLocation();
        }else{
            mLocationClient.stopLocation();
        }
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if(aMapLocation != null){
            mainFragment.upDateLocation(aMapLocation.getLatitude(), aMapLocation.getLongitude(), "BEIJING");
        }else{
            mainFragment.upDateLocation(-1, -1, "BEIJING");
            Toast.makeText(MainActivity.this,ResourcesHelper.getString(MainActivity.this,R.string.message_error_no_location),Toast.LENGTH_SHORT).show();
        }
    }

    public void initFragment(){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        historyFragment = HistoryFragment.newInstance();
        mainFragment = MainFragment.newInstance();
        fragmentTransaction.add(R.id.main_container, mainFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (checkedId == R.id.radio_index) {
            if(!mainFragment.isAdded()){
                fragmentTransaction.hide(historyFragment).add(R.id.main_container,mainFragment);
            }else{
                fragmentTransaction.hide(historyFragment).show(mainFragment);
            }
            fragmentTransaction.commit();
        } else if (checkedId == R.id.radio_history) {
            if(!historyFragment.isAdded()){
                fragmentTransaction.hide(mainFragment).add(R.id.main_container,historyFragment);
            }else{
                fragmentTransaction.hide(mainFragment).show(historyFragment);
            }
            fragmentTransaction.commit();
        } else if (checkedId == R.id.radio_news) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
            if((System.currentTimeMillis() - exitTime) > 2000){
                Toast.makeText(this,getResources().getString(R.string.message_exit_application),Toast.LENGTH_LONG).show();
                exitTime = System.currentTimeMillis();
            }else{
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PermissionHelper.REQUEST_ALL:
                String permission = null;
                int grant = -1;
                for(int i = 0; i < permissions.length; i ++){
                    if(PermissionHelper.PERMISSION_COARSE_LOCATION.equals(permissions[i])){
                        permission = permissions[i];
                        grant = grantResults[i];
                        break;
                    }
                }
                if(grant == PackageManager.PERMISSION_GRANTED && PermissionHelper.PERMISSION_COARSE_LOCATION.equals(permission)){
                    mLocationClient.startLocation();
                }else if(grant == PackageManager.PERMISSION_DENIED && PermissionHelper.PERMISSION_COARSE_LOCATION.equals(permission)){
                    mainFragment.upDateLocation(-1, -1, "BEIJING");
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        mLocationClient.stopLocation();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mLocationClient.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if(mSharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND,false)
                || mSharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND,false)){

            btnRemind.setBackground(getResources().getDrawable(R.drawable.btn_float_has_remind_bg));
        }else{
            btnRemind.setBackground(getResources().getDrawable(R.drawable.btn_float_no_remind_bg));
        }
        super.onResume();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
