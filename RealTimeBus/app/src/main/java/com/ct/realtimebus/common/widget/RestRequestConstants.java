package com.ct.realtimebus.common.widget;

/**
 * Created by zhangjinli on 16/8/2.
 */
public class RestRequestConstants {
//    private static final String SERVICE = "http://172.17.100.130";
//    private static final String PORT = "7080";
    private static final String SERVICE = "http://42.159.204.10";
    private static final String PORT = "8080";
    private static final String BASE_URL = SERVICE + ":" + PORT;
    private static final String URL = BASE_URL + "/mobility/api";

    /**
     * 查询定点位置附近所有站点
     */
    public static final String MAIN_NEAR_STATION_BY_LOCATION = URL + "/suggest/stop";

    /**
     * 查询含有搜索词的所有公交站
     */
    public static final String MAIN_SEARCH_STATION_BY_KEYWORDS = URL + "/suggest/stop/group";

    /**
     * 根据站点ID查询线路
     */
    public static final String MAIN_SEARCH_STATION_BY_STATION_ID = URL + "/service/stop/{0}/lines";

    /**
     * 根据线路ID查询线路详情
     */
    public static final String LINE_INFO_BY_LINE_ID = URL + "/service/line/{0}";//?stop=558&stops=1&path=1&size=5 参数

    /**
     * 根据线路ID查询该线路经过的所有站点
     */
    public static final String SEARCH_STOPS_BY_LINE_ID = URL + "/service/line/{0}";

    /**
     * 根据busId查询公交详细信息
     */
    public static final String BUS_INFO_BY_BUS_ID = URL + "/service/bus/{0}";
}
