package com.ct.realtimebus.mvp.model;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.TestData;
import com.ct.realtimebus.common.VolleyHelper;
import com.ct.realtimebus.common.widget.RestRequestConstants;
import com.ct.realtimebus.mvp.model.inf.IBusModel;
import com.ct.realtimebus.test.MyTestSingle;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/9/6.
 */
public class BusModel implements IBusModel {
    private Context context;

    @Override
    public void getBusInfoById(int id, final ImplDataResultListener onDataResultListener) {
        if(!TestData.isOpenDemoData(context)){
            final VolleyHelper volleyHelper = new VolleyHelper(context);
            String url = RestRequestConstants.BUS_INFO_BY_BUS_ID;
            url = MessageFormat.format(url,id+"",0);
            volleyHelper.createRequest(url, Request.Method.GET, new HashMap<String, String>(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String,String> resultMap = VolleyHelper.checkResult(response);
                        if(VolleyHelper.isResultError(resultMap)){
                            Toast.makeText(context, ResourcesHelper.getString(context, R.string.message_error_request),Toast.LENGTH_SHORT).show();
                        }else{
                            onDataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(context, ResourcesHelper.getString(context, R.string.message_error_request),Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onDataResultListener.onFailure(ResourcesHelper.getString(context, R.string.message_error_request),new Exception());
                }
            });
        }else{
            MyTestSingle mts = MyTestSingle.getInstance();
            Gson gson = new Gson();
            if(mts.getNearBus() != null){
                onDataResultListener.onSuccess(gson.toJson(mts.getNearBus()));
            }else{
                JSONObject comeBus = TestData.getAssetsJson(context, "busBell.txt");
                try {
                    BeanBus nearBus = gson.fromJson(comeBus.getJSONObject("result").toString(), BeanBus.class);
                    mts.setNearBus(nearBus);
                    onDataResultListener.onSuccess(comeBus.getJSONObject("result").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                TestData.getTestData(context,"comebus.txt",onDataResultListener);
            }
        }
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
}
