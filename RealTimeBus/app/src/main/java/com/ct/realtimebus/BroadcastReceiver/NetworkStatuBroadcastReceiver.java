package com.ct.realtimebus.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.RTBUtils;

/**
 * Created by zhangjinli on 16/8/18.
 */
public class NetworkStatuBroadcastReceiver extends BroadcastReceiver {

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        if(RTBUtils.checkNetwork(context)){
            toast(mContext.getResources().getString(R.string.message_error_no_network));
        }
    }

    public void toast(String message){
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }
}
