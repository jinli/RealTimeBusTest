package com.ct.realtimebus.common;

import android.app.ProgressDialog;
import android.content.Context;

import com.ct.realtimebus.R;

/**
 * Created by zhangjinli on 16/8/18.
 */
public class DialogUtils {

    public static ProgressDialog showLoading(Context context){
        ProgressDialog dialog = ProgressDialog.show(context,"",context.getResources().getString(R.string.message_loading));
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }
}
