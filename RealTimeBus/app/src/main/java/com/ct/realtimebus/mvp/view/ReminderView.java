package com.ct.realtimebus.mvp.view;

import org.json.JSONObject;

/**
 * Created by zhangjinli on 16/8/16.
 */
public interface ReminderView {

    void initReminderSetting(JSONObject jsonObject);
}
