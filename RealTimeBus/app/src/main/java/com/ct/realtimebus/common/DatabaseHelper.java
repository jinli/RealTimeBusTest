package com.ct.realtimebus.common;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ct.realtimebus.common.dbmanager.HistoryDBManager;

/**
 * Created by zhangjinli on 16/8/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper{
    private static final String TAG = "DATAbaseHelper";

    private SQLiteDatabase mdb;
    /**数据库名称**/
    private static final String DB_NAME = "RealTimeBus.db";
    /**数据库版本号**/
    private static final int DB_VERSION = 1;

    /**历史纪录表名称**/
    public static final String TABLE_HISTORY = "History";

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context){
        this(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.mdb = db;
        String sql = "CREATE TABLE IF NOT EXISTS "+TABLE_HISTORY+" (" +
                HistoryDBManager.COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                HistoryDBManager.COLUMN_STOP_ID + " INTEGER NOT NULL," +
                HistoryDBManager.COLUMN_STOP_NAME+" VARCHAR NOT NULL," +
                HistoryDBManager.COLUMN_LINE_ID + " INTEGER NOT NULL," +
                HistoryDBManager.COLUMN_LINE_NAME + " VARCHAR NOT NULL," +
                HistoryDBManager.COLUMN_CREATE_DATE + " NOT NULL," +
                HistoryDBManager.COLUMN_FROM_STOP_NAME + " VARCHAR NOT NULL," +
                HistoryDBManager.COLUMN_TO_STOP_NAME + " VARCHAR NOT NULL," +
                HistoryDBManager.COLUMN_SUBSCRIBE_OR_READ + " INTEGER NOT NULL," +
                HistoryDBManager.COLUMN_SOR_TIMES+" INTEGER NOT NULL)";
        db.execSQL(sql);
        Log.i(TAG,"首次创建数据库表:"+TABLE_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO 数据库版本更新时使用的方法
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        //TODO 每次打开数据库后首先执行该方法，可以做一些初始化工作
    }
}
