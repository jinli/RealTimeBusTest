package com.ct.realtimebus.reminder.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanChooiceLine;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.reminder.activity.ReminderSettingActivity;
import com.ct.realtimebus.reminder.adapter.RemindBusLineListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/14.
 */
public class BusLineListDialogFragment extends DialogFragment {

    private static final String TAG = "BusLineListDialogFragment";
    private static final String STOP = "stop";
    private static final String LINES = "lines";

    private RecyclerView recyclerViewBusLine;

    private RemindBusLineListAdapter remindBusLineListAdapter;

    private BeanStop stop;
    private ArrayList<BeanChooiceLine> allLines;
    private ArrayList<BeanChooiceLine> chooiceLines = new ArrayList<>();

    public BusLineListDialogFragment() {
    }

    public static BusLineListDialogFragment newInstance(BeanStop stop,ArrayList<BeanChooiceLine> allLines) {
        BusLineListDialogFragment busLineListDialogFragment = new BusLineListDialogFragment();
        Bundle bundle = new Bundle();
        busLineListDialogFragment.setArguments(bundle);
        bundle.putSerializable(STOP, stop);
        bundle.putSerializable(LINES,allLines);
        return busLineListDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bus_list_dialog, container, false);
        recyclerViewBusLine = (RecyclerView) view.findViewById(R.id.recycler_bus_line);
        /**整理出来当前要选择的线路信息*/
        ArrayList<BeanLine> currentLines = stop.getLines();
        for(BeanLine beanLine : currentLines){
            for(BeanChooiceLine beanChooiceLine : allLines){
                if(beanLine.getLine().equals(beanChooiceLine.getLineName())){
                    chooiceLines.add(beanChooiceLine);
                }
            }
        }

        remindBusLineListAdapter = new RemindBusLineListAdapter(getActivity(),chooiceLines);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewBusLine.setLayoutManager(linearLayoutManager);
        recyclerViewBusLine.setAdapter(remindBusLineListAdapter);
        remindBusLineListAdapter.setItemClickListener(new RemindBusLineListAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position, int subPosition) {
                BeanLine beanLine = chooiceLines.get(position).getLines().get(subPosition);
                if (beanLine.getBuses() != null && beanLine.getBusSize() > 0) {
                    Intent intent = new Intent(getActivity(),ReminderSettingActivity.class);
                    intent.putExtra(Constants.IntentKey.REMIND_LINE,beanLine);
                    intent.putExtra(Constants.IntentKey.REMIND_STOP,stop);
                    startActivity(intent);
                    dismiss();
                }else{
                    noBusToast();
                }
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.stop = (BeanStop) getArguments().getSerializable(STOP);
            this.allLines = (ArrayList<BeanChooiceLine>)getArguments().getSerializable(LINES);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void noBusToast() {
        Toast.makeText(getActivity(), getResources().getString(R.string.message_error_no_bus), Toast.LENGTH_SHORT).show();
    }
}
