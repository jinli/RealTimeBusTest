package com.ct.realtimebus.mvp.model;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.TestData;
import com.ct.realtimebus.common.VolleyHelper;
import com.ct.realtimebus.common.widget.RestRequestConstants;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;
import com.ct.realtimebus.test.MyTestSingle;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import junit.framework.Test;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public class LineModel implements ILineModel {

    private Context mContext;
    private Handler mHandler;

    @Override
    public void searchLinesByStopId(int stationId, HashMap<String, String> params, final ImplDataResultListener onDataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            String url = RestRequestConstants.MAIN_SEARCH_STATION_BY_STATION_ID;
            url = MessageFormat.format(url, stationId + "");
            VolleyHelper volleyHelper = new VolleyHelper(mContext);
            volleyHelper.createRequest(url, Request.Method.GET, params, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String, String> resultMap = VolleyHelper.checkResult(response);
                        if (VolleyHelper.isResultError(resultMap)) {
                            onDataResultListener.onFailure(resultMap.get(VolleyHelper.MESSAGE), null);
                        } else {
                            onDataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), null);
                }
            });
        } else {
            TestData.getTestData(mContext, "search.txt", onDataResultListener);
        }
    }

    @Override
    public void getLineInfoByIds(final ArrayList<Integer> lineIds, final HashMap<String, String> params, final ImplDataResultListener onDataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    HttpURLConnection conn = null;
                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    JSONArray lines = new JSONArray();
                    for (int i = 0; i < lineIds.size(); i++) {
                        String urlStr = RestRequestConstants.LINE_INFO_BY_LINE_ID;
                        String paramsStr = createParams(params);
                        try {
                            urlStr = MessageFormat.format(urlStr, lineIds.get(i) + "");
                            //创建Http请求
                            URL url = new URL(urlStr + paramsStr);
                            conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setConnectTimeout(30000);
                            conn.setReadTimeout(5000);
                            int code = conn.getResponseCode();
                            if (code == 200) {
                                InputStream is = conn.getInputStream();
                                String result = getStringFromInputStream(is);
                                JSONObject line = new JSONObject(result);
                                HashMap<String, String> resultMap = VolleyHelper.checkResult(line.toString());
                                if (!resultMap.containsKey(VolleyHelper.MESSAGE)) {
                                    lines.put(new JSONObject(resultMap.get(VolleyHelper.RESULT)));
                                } else {
                                    bundle.putString("error", "");
                                    message.setData(bundle);
                                    mHandler.sendMessage(message);
                                    break;
                                }
                            } else {
                                bundle.putInt("error", code);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            conn.disconnect();
                        }
                    }
                    conn.disconnect();
                    bundle.putString("result", lines.toString());
                    message.setData(bundle);
                    mHandler.sendMessage(message);
                }
            });
            thread.start();

            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    Bundle bundle = msg.getData();
                    if (!bundle.containsKey("error")) {
                        String result = bundle.getString("result");
                        onDataResultListener.onSuccess(result);
                    }
                    super.handleMessage(msg);
                }
            };
        }else{
            JSONObject comeBus = TestData.getAssetsJson(mContext,"comebus.txt");
            JSONArray lines = new JSONArray();
            try {
                lines.put(comeBus.getJSONObject("result"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            onDataResultListener.onSuccess(lines.toString());
        }
    }



    @Override
    public void getLineInfoById(int lineId, HashMap<String, String> params, final ImplDataResultListener dataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            VolleyHelper volleyHelper = new VolleyHelper(mContext);
            String urlStr = RestRequestConstants.LINE_INFO_BY_LINE_ID;
            urlStr = MessageFormat.format(urlStr, lineId + "");
            volleyHelper.createRequest(urlStr, Request.Method.GET, params, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String, String> resultMap = VolleyHelper.checkResult(response);
                        if (VolleyHelper.isResultError(resultMap)) {
                            dataResultListener.onFailure(resultMap.get(VolleyHelper.MESSAGE), null);
                        } else {
                            dataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), null);
                }
            });
        } else {
            TestData.getTestData(mContext, "comebus.txt", dataResultListener);
        }
    }

    @Override
    public void getRealTimeBus(int lineId, HashMap<String,String> params, final ImplDataResultListener onDataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            VolleyHelper volleyHelper = new VolleyHelper(mContext);
            String urlStr = RestRequestConstants.LINE_INFO_BY_LINE_ID;
            urlStr = MessageFormat.format(urlStr, lineId + "");
            volleyHelper.createRequest(urlStr, Request.Method.GET, params, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String, String> resultMap = VolleyHelper.checkResult(response);
                        if (VolleyHelper.isResultError(resultMap)) {
                            onDataResultListener.onFailure(resultMap.get(VolleyHelper.MESSAGE), null);
                        } else {
                            onDataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), null);
                }
            });
        }else{
            MyTestSingle mts = MyTestSingle.getInstance();
            try {
                JSONObject comeBus = TestData.getAssetsJson(mContext,"comebus.txt");
//                JSONArray buses = comeBus.getJSONObject("result").getJSONArray("buses");
                if(mts.getNearBus() != null){
//                    Gson gson = new Gson();
//                    for(int i = 0; i < buses.length(); i ++){
//                        JSONObject bus = buses.getJSONObject(i);
//                        if(bus.getInt("id") == mts.getNearBus().getId()){
//                            buses.remove(i);
//                            gson.toJson(mts.getNearBus());
//                            buses.put(0,gson.toJson(mts.getNearBus()));
//                        }
//                    }
//                    comeBus.getJSONObject("result").put("buses",buses);

                    ArrayList<BeanBus> buses = new ArrayList<>();
                    buses.add(mts.getNearBus());
                    Gson gson = new Gson();
                    onDataResultListener.onSuccess(gson.toJson(buses));
                }else{
                    onDataResultListener.onSuccess(comeBus.getJSONObject("result").getJSONArray("buses").toString());
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }


    public synchronized String createParams(HashMap<String, String> params) {

        String regex = "[\u4E00-\u9FA5]+";
        String strParams = "?";
        int j = 0;
        for (String key : params.keySet()) {
            String param = params.get(key);
            strParams = strParams + key + "=";
            if (param.matches(regex)) {
                try {
                    strParams = strParams + URLEncoder.encode(param, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                strParams = strParams + param;
            }
            j++;
            if (j < params.size()) {
                strParams = strParams + "&";
            }

        }
        return strParams;
    }

    /**
     * 通过字节输入流返回一个字符串信息
     *
     * @param is
     * @return
     * @throws IOException
     */
    private static String getStringFromInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = is.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        is.close();
        String status = baos.toString();
        baos.close();
        return status;
    }

    @Override
    public void setContext(Context context) {
        this.mContext = context;
    }
}
