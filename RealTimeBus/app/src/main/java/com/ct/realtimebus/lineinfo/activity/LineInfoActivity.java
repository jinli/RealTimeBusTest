package com.ct.realtimebus.lineinfo.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BaseActivity;
import com.ct.realtimebus.common.BusHelper;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.DialogUtils;
import com.ct.realtimebus.common.MyApplication;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.TestData;
import com.ct.realtimebus.lineinfo.adapter.BusTimeLineAdapter;
import com.ct.realtimebus.mvp.presenter.LineInfoPresenter;
import com.ct.realtimebus.mvp.view.LineInfoView;
import com.ct.realtimebus.main.LruHistory;
import com.ct.realtimebus.reminder.activity.ReminderSettingActivity;
import com.ct.realtimebus.test.MyTestSingle;
import com.ct.realtimebus.test.TestButtonActivity;
import com.google.gson.Gson;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by zhangjinli on 16/8/4.
 */
public class LineInfoActivity extends BaseActivity implements View.OnClickListener, LineInfoView,View.OnLongClickListener{

    private static final String TAG = "LineInfoActivity";
    private static final String PARAM_STOP = "stop";
    private static final String PARAM_STOPS = "stops";
    private static final String PARAM_PATH = "path";
    private static final String PARAM_SIZE = "size";
    private HashMap<String,String> params = new HashMap<>();
    private ArrayList<RadioButton> radioButtonArrayList = new ArrayList<>();

    private LineInfoPresenter lineInfoPresenter;

    private Intent mIntent;

    private TextView tvTitle;
    private RadioGroup rgLineSwitch;
    private View animationLine;
    private ImageButton btnLineMap;
    private ImageButton btnBack;
    private RecyclerView recyclerBusTimeLine;
    private ImageButton btnReminder;
    private View viewMiddleLine;
    private ProgressDialog progressDialog;
    private BusTimeLineAdapter busTimeLineAdapter;
    private ArrayList<Integer> lineIds;
    private ArrayList<BeanLine> lines;
    private ArrayList<BeanBus> buses;
    private BeanStop currentStop;
    private BeanLine currentLine;
    private int stopId;
    private int lineIndex = 0;
    private Timer timer = new Timer();
    private MyTimerTask timerTask;
    private LinearLayoutManager linearLayoutManager;
    private SharedPreferences mSharedPreferences;
    private long timePeriod = 1100;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            lineInfoPresenter.getRealTimeBus(currentLine.getId(), params);
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_info);
        mSharedPreferences = getSharedPreferences(Constants.SHARED_REMIND, 0);
        btnReminder = (ImageButton) findViewById(R.id.btn_reminder);
        viewMiddleLine = findViewById(R.id.view_middle_line);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btnLineMap = (ImageButton) findViewById(R.id.btn_line_map);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnLineMap.setVisibility(View.VISIBLE);
        btnLineMap.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnReminder.setOnClickListener(this);
        btnReminder.setOnLongClickListener(this);
        lineInfoPresenter = new LineInfoPresenter(getApplicationContext(), this);

        //初始化计时器
        initTimerTask();

        mIntent = getIntent();
        Bundle bundle = mIntent.getExtras();
        lineIds = (ArrayList<Integer>) bundle.getSerializable(Constants.IntentKey.LINE_ID);
        currentStop = (BeanStop) bundle.getSerializable(Constants.IntentKey.STOP_ID);
        stopId = currentStop.getId();
        initTabAnimation();//初始化Tab切换动画

        //初始化列表
        recyclerBusTimeLine = (RecyclerView) findViewById(R.id.recycler_bus_time_line);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerBusTimeLine.setLayoutManager(linearLayoutManager);

        //设置参数
        params.put(PARAM_STOP, stopId + "");
        params.put(PARAM_STOPS, "1");
        params.put(PARAM_PATH, "1");
        params.put(PARAM_SIZE, "5");
        progressDialog = DialogUtils.showLoading(this);
        lineInfoPresenter.getLineInfoById(lineIds, params);
    }

    @Override
    public void initBusLineTimeLine(ArrayList<BeanLine> resultLines) {
        this.lines = resultLines;
        if (lines != null && lines.size() > 1) {//当线路是对向
            for (int i = 0; i < lines.size(); i++) {
                RadioButton radiobutton = radioButtonArrayList.get(i);
                radiobutton.setText(lines.get(i).getTerminal());
            }
        } else {//当线路是环线
            RadioButton radiobutton = radioButtonArrayList.get(0);
            radiobutton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.once_line), null, null, null);
            radiobutton.setText(lines.get(0).getTerminal());
            animationLine.setVisibility(View.GONE);
            viewMiddleLine.setVisibility(View.GONE);
            radioButtonArrayList.get(1).setVisibility(View.GONE);
        }

        ArrayList<BeanStop> stops = lines.get(lineIndex).getStops();
        currentLine = lines.get(lineIndex);
        tvTitle.setText(lines.get(lineIndex).getLine());
        buses = currentLine.getBuses();
        /**由于后台数据不正确，需要单独整理buses**/
        buses = new ArrayList<>();
        for(BeanBus bus : currentLine.getBuses()){
            if(bus.getLineId() == currentLine.getId()){
                buses.add(bus);
            }
        }

//        MyTestSingle mt = MyTestSingle.getInstance();
//        BeanBus nearBus = BusHelper.getNearBus(buses, currentStop);
//        mt.setNearBus(nearBus);
//        currentLine.setAllStops(currentLine.getStops());
        /**由于后台数据不正确，需要单独整理buses**/
        busTimeLineAdapter = new BusTimeLineAdapter(this, currentLine.getId(), stops, buses, currentStop);
        recyclerBusTimeLine.setAdapter(busTimeLineAdapter);
        //TODO 启动更新bus状态的Timer
        timer.schedule(timerTask, 1000, MyTestSingle.period);
        //保存历史纪录
        saveHistory();
        progressDialog.dismiss();
    }

    @Override
    public void updateTimeBusStatus(ArrayList<BeanBus> resultBuses) {
//        buses = resultLine.getBuses();
//        Log.v(TAG,resultBuses.get(0).getLatitude()+"==========");
//        Log.v(TAG,resultBuses.get(0).getLongitude()+"==========");
//        Log.v(TAG,resultBuses.get(0).getDistance()+"===============");
        buses = new ArrayList<>();
        for(BeanBus bus : resultBuses){
            if(bus.getLineId() == currentLine.getId()){
                buses.add(bus);
            }
        }
        int position = busTimeLineAdapter.setBuses(buses);
        busTimeLineAdapter.notifyDataSetChanged();
        recyclerBusTimeLine.scrollToPosition(position);
    }

    public void saveHistory() {
        MyApplication.getInstance().addHistory(currentStop, currentLine, LruHistory.READ);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_line_map:
                Intent intent = new Intent(this, LineMapActivity.class);
                intent.putExtra(Constants.IntentKey.LINE_DETAIL, lines.get(lineIndex));
                intent.putExtra(Constants.IntentKey.STOP_DETAIL, currentStop);
                startActivity(intent);
                break;
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_reminder:
                Gson gson = new Gson();
                /**当前有上车提醒或下车提醒**/
                if (mSharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false) ||
                        mSharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {
                    BeanLine remindLine = gson.fromJson(mSharedPreferences.getString(Constants.SharedRemindKey.REMIND_LINE, ""), BeanLine.class);
                    /**
                     * 如果当前线路与提醒中的线路不是同一条线路则弹出选择Dialog
                     * 如果相同则检查该线路是否有公交车，有公交车则跳转到提醒设置界面，如果没有弹出Toast
                     **/
                    if (currentLine.getId() != remindLine.getId()) {
                        showChooiceDialog();
                    } else {
                        if (buses != null && buses.size() > 0) {
                            Intent remindIntent = new Intent(LineInfoActivity.this, ReminderSettingActivity.class);
                            remindIntent.putExtra(Constants.IntentKey.REMIND_STOP, currentStop);
                            remindIntent.putExtra(Constants.IntentKey.REMIND_LINE, currentLine);
                            startActivity(remindIntent);
//                            SharedPreferences.Editor editor = mSharedPreferences.edit();
//                            editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
//                            editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
//                            editor.commit();
                        } else {
                            Toast.makeText(LineInfoActivity.this, getResources().getString(R.string.message_error_no_bus), Toast.LENGTH_SHORT).show();
                        }
                    }
                    /**既没有上车提醒也没有下车提醒**/
                } else if (!mSharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false) &&
                        !mSharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {
                    if (buses != null && buses.size() > 0) {
                        Intent remindIntent = new Intent(LineInfoActivity.this, ReminderSettingActivity.class);
                        remindIntent.putExtra(Constants.IntentKey.REMIND_STOP, currentStop);
                        remindIntent.putExtra(Constants.IntentKey.REMIND_LINE, currentLine);
                        startActivity(remindIntent);
//                        SharedPreferences.Editor editor = mSharedPreferences.edit();
//                        editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
//                        editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
//                        editor.commit();
                    } else {
                        Toast.makeText(LineInfoActivity.this, getResources().getString(R.string.message_error_no_bus), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    /**
     * 选择弹窗
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showChooiceDialog() {
        final Gson gson = new Gson();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialog_change_line_confirm);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setWindowAnimations(R.style.WindowInOutTranslate);
        dialog.show();
        Button btnChange = (Button) dialog.findViewById(R.id.btn_change);
        Button btnCancleRemind = (Button) dialog.findViewById(R.id.btn_cancle_remind);
        Button btnCancle = (Button) dialog.findViewById(R.id.btn_cancle);
        String btnText = getResources().getString(R.string.actionChangeLine);
        btnText = MessageFormat.format(btnText, currentLine.getLine());
        btnChange.setText(btnText);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentLine.getBusSize() > 0) {
                    Intent remindIntent = new Intent(LineInfoActivity.this, ReminderSettingActivity.class);
                    remindIntent.putExtra(Constants.IntentKey.REMIND_STOP, currentStop);
                    remindIntent.putExtra(Constants.IntentKey.REMIND_LINE, currentLine);
                    startActivity(remindIntent);
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
                    editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
                    editor.commit();
                } else {
                    Toast.makeText(LineInfoActivity.this, getResources().getString(R.string.message_error_no_bus), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        btnCancleRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
                editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
                editor.commit();
                onResume();
                dialog.dismiss();
                Toast.makeText(LineInfoActivity.this, ResourcesHelper.getString(LineInfoActivity.this, R.string.nessage_success_remind_cancle), Toast.LENGTH_SHORT).show();

            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void initTimerTask() {
        timerTask = new MyTimerTask();
    }

    public void initTabAnimation() {
        rgLineSwitch = (RadioGroup) findViewById(R.id.rg_line_switch);
        radioButtonArrayList.add((RadioButton) findViewById(R.id.radio_one));
        radioButtonArrayList.add((RadioButton) findViewById(R.id.radio_tow));
        animationLine = findViewById(R.id.animation_line);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) animationLine.getLayoutParams();
        layoutParams.height = 10;
        long duration = 200;//动画执行时间
        layoutParams.width = getWindowManager().getDefaultDisplay().getWidth() / 2;
        animationLine.setLayoutParams(layoutParams);
        final TranslateAnimation leftToRight = new TranslateAnimation(0, getWindowManager().getDefaultDisplay().getWidth() / 2,
                TranslateAnimation.RELATIVE_TO_SELF, 0f);
        leftToRight.setDuration(duration);
        leftToRight.setFillAfter(true);
        final TranslateAnimation rightToLeft = new TranslateAnimation(getWindowManager().getDefaultDisplay().getWidth() / 2, 0,
                TranslateAnimation.RELATIVE_TO_SELF, 0f);
        rightToLeft.setDuration(duration);
        rightToLeft.setFillAfter(true);
        for (RadioButton radioButton : radioButtonArrayList) {
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getId() == R.id.radio_one && isChecked) {
                        animationLine.startAnimation(rightToLeft);
                        lineIndex = 0;
                        switchLine();
                        radioButtonArrayList.get(0).setChecked(true);
                        radioButtonArrayList.get(1).setChecked(false);
                    } else if (buttonView.getId() == R.id.radio_tow && isChecked) {
                        animationLine.startAnimation(leftToRight);
                        lineIndex = 1;
                        switchLine();
                        radioButtonArrayList.get(1).setChecked(true);
                        radioButtonArrayList.get(0).setChecked(false);
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (timer == null) {
            timer = new Timer();
            timerTask = new MyTimerTask();
            timer.schedule(timerTask, 1000, MyTestSingle.period);

            if (mSharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)
                    || mSharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {
                btnReminder.setBackground(getResources().getDrawable(R.drawable.btn_float_has_remind_bg));
            } else {
                btnReminder.setBackground(getResources().getDrawable(R.drawable.btn_float_no_remind_bg));
            }
        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        if (timer == null) {
            timer = new Timer();
            timerTask = new MyTimerTask();
            timer.schedule(timerTask, 1000, MyTestSingle.period);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    /**
     * 线路切换
     */
    public void switchLine() {
        if (lineIndex <= lines.size() - 1) {
            currentLine = lines.get(lineIndex);
            busTimeLineAdapter.updateData(currentLine.getStops());
            busTimeLineAdapter.updateCurrentLineId(currentLine.getId());
            buses = new ArrayList<>();
            for(BeanBus bus : currentLine.getBuses()){
                if(bus.getLineId() == currentLine.getId()){
                    buses.add(bus);
                }
            }
            busTimeLineAdapter.setBuses(buses);
            busTimeLineAdapter.notifyDataSetChanged();
            //保存历史纪录
            saveHistory();
        }
    }

    @Override
    public boolean onLongClick(View v) {
        Intent intent = new Intent(this, TestButtonActivity.class);
        startActivity(intent);
        return false;
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            handler.sendEmptyMessage(0);
        }
    }
}
