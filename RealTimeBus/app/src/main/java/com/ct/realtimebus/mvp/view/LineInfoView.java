package com.ct.realtimebus.mvp.view;

import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/8.
 */
public interface LineInfoView {

    void initBusLineTimeLine(ArrayList<BeanLine> lines);

    void updateTimeBusStatus(ArrayList<BeanBus> line);
}
