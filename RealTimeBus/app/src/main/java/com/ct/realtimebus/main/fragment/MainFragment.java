package com.ct.realtimebus.main.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanChooiceLine;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.DialogUtils;
import com.ct.realtimebus.common.RTBUtils;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.widget.IconFontButton;
import com.ct.realtimebus.common.widget.SpaceItemDecoration;
import com.ct.realtimebus.main.activity.MainActivity;
import com.ct.realtimebus.mvp.view.MainView;
import com.ct.realtimebus.main.activity.SearchActivity;
import com.ct.realtimebus.main.adapter.InitBusLineRecyclerAdapter;
import com.ct.realtimebus.main.adapter.SearchBusLineRecyclerAdapter;
import com.ct.realtimebus.mvp.presenter.MainPresenter;
import com.ct.realtimebus.reminder.activity.ReminderSettingActivity;
import com.ct.realtimebus.reminder.fragment.BusLineListDialogFragment;
import com.ct.realtimebus.test.MyTestSingle;
import com.ct.realtimebus.test.TestButtonActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements View.OnClickListener,View.OnLongClickListener ,MainView {

    private static final String TAG = "MainFragment";
    public static final int SEARCH_STOP_REQUESTCODE = 0;//检索界面跳转标识
    public static final int SEARCH_STOP_RESULTCODE = 1;//返回标识

    private MainPresenter mainPresenter;
    private HashMap<String, String> params = new HashMap<>();

    private static final String LNG = "lng";
    private static final String LAT = "lat";
    private static final String CITY = "city";
    private static final String LENGTH = "length";
    private static final String QUERY = "q";

    private TextView locationNameTextView;
    private EditText topSearchEdit;
    private RecyclerView recycler_bus;
    private LinearLayout switchStop;
    private ImageButton btnReminder;
    private ViewStub viewStub;
    private IconFontButton btnRelocation;

    private ProgressDialog loadingDialog;

    private View fragmentMainView;

    private OnFragmentInteractionListener mListener;

    private InitBusLineRecyclerAdapter initBusLineRecyclerAdapter;
    private SearchBusLineRecyclerAdapter searchBusLineRecyclerAdapter;

    private double lat;
    private double lng;
    private String city;
    private int length = 5;

    private ArrayList<BeanStop> stops;//初始化列表stops；
    private BeanStop currentStop;//当前stop；
    private ArrayList<BeanChooiceLine> chooiceLines;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences testSp;


    public MainFragment() {

    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainPresenter = new MainPresenter(getActivity(), this);
        mSharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_REMIND, 0);
        testSp = getActivity().getSharedPreferences(TestButtonActivity.TEST_SETTING, 0);
        params.put(QUERY, "");
        params.put(LENGTH, length + "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentMainView = inflater.inflate(R.layout.fragment_main, container, false);
        switchStop = (LinearLayout)fragmentMainView.findViewById(R.id.switch_stop);
        topSearchEdit = (EditText) fragmentMainView.findViewById(R.id.top_search_edit);
        btnReminder = (ImageButton) getActivity().findViewById(R.id.btn_reminder);
        viewStub = (ViewStub) fragmentMainView.findViewById(R.id.vs_msg);
        btnRelocation = (IconFontButton)fragmentMainView.findViewById(R.id.relocation);

        //判断是否有网络
        if (!RTBUtils.checkNetwork(getActivity())) {
            showErrorView(R.id.no_netWork);
        }
        topSearchEdit.setFocusable(false);
        topSearchEdit.setOnClickListener(this);
        btnReminder.setOnClickListener(this);
        btnReminder.setOnLongClickListener(this);
        switchStop.setOnClickListener(this);

        locationNameTextView = (TextView) fragmentMainView.findViewById(R.id.location_name_textview);
        recycler_bus = (RecyclerView) fragmentMainView.findViewById(R.id.recycler_bus);
        recycler_bus.setLayoutManager(new LinearLayoutManager(getActivity()));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.main_activity_bus_list_item_space);
        recycler_bus.addItemDecoration(new SpaceItemDecoration(spacingInPixels));
        //初始化检索结果列表数据适配器
        searchBusLineRecyclerAdapter = new SearchBusLineRecyclerAdapter(getActivity());
        loadingDialog = DialogUtils.showLoading(getActivity());
        initWidget();
        return fragmentMainView;
    }

    private void initWidget(){
        topSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString() != null && !"".equals(s.toString())) {
                    ((MainActivity) getActivity()).locationSwitch(false);
                    btnRelocation.setVisibility(View.VISIBLE);
                } else if (s.toString() == null || "".equals(s.toString())) {
                    ((MainActivity) getActivity()).locationSwitch(true);
                    btnRelocation.setVisibility(View.GONE);
                }
            }
        });

        btnRelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = topSearchEdit.getText().toString();
                if (s != null && !"".equals(s)) {
                    topSearchEdit.setText("");
                    ((MainActivity) getActivity()).locationSwitch(true);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SEARCH_STOP_REQUESTCODE && resultCode == SEARCH_STOP_RESULTCODE) {
            stops = (ArrayList<BeanStop>) data.getSerializableExtra("searchStop");
            initBusLineRecyclerAdapter.updateStops(stops);
            currentStop = initBusLineRecyclerAdapter.getCurrentStop();
            updateWidget(true);
            params.put("buses", "1");
            mainPresenter.searchLinesByStationId(currentStop.getId(), params);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (testSp.getBoolean(TestButtonActivity.DEMO_DATA, false)) {
            mainPresenter.getBusStationList(params);
        }
        /**测试数据初始化**/
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_REMIND, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
        editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
        editor.commit();
        if (!sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)
                && !sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)) {
            MyTestSingle mts = MyTestSingle.getInstance();
            mts.setNearBus(null);
        }
    }


        /**
         * 初始化Fragment中的列表数据
         *
         * @param lat
         * @param lng
         * @param city
         */
    public void upDateLocation(double lat, double lng, String city) {
        if (lat == -1 && lng == -1) {
            loadingDialog.dismiss();
            showErrorView(R.id.no_location);
        } else {
            this.lat = lat;
            this.lng = lng;
            this.city = city;
            params.put(CITY, city);
            params.put(LNG, lng + "");
            params.put(LAT, lat + "");
            mainPresenter.getBusStationList(params);

        }
    }

    /**
     * 初始化附近车站列表
     */
    @Override
    public void initBusStationList(ArrayList<BeanStop> stops,HashMap<String,ArrayList<Integer>> lineIdMap,ArrayList<BeanChooiceLine> chooiceLines) {
        this.stops = stops;
        this.chooiceLines = chooiceLines;
        initBusLineRecyclerAdapter = new InitBusLineRecyclerAdapter(getActivity(), stops,lineIdMap);
        recycler_bus.setAdapter(initBusLineRecyclerAdapter);
        currentStop = initBusLineRecyclerAdapter.getCurrentStop();
        updateWidget(false);
        if (loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    /**
     * 更新线路列表
     *
     * @param lines
     */
    @Override
    public void updateBusRecycler(ArrayList<BeanLine> lines) {
        if(lines != null){
            stops.get(initBusLineRecyclerAdapter.getStopPosition()).setLines(lines);
            initBusLineRecyclerAdapter.notifyDataSetChanged();
            currentStop = initBusLineRecyclerAdapter.getCurrentStop();
            updateWidget(false);
            if(lines.size() == 0){
                Toast.makeText(getActivity(), ResourcesHelper.getString(getActivity(),R.string.message_no_line),Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showErrorView(int viewId) {
        if(!testSp.getBoolean(TestButtonActivity.DEMO_DATA, false)){
            if (!viewStub.isInLayout()) {
                viewStub.inflate();
            }
            fragmentMainView.findViewById(viewId).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.top_search_edit) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra("lat", lat);
            intent.putExtra("lng", lng);
            intent.putExtra("city", city);
            startActivityForResult(intent, SEARCH_STOP_REQUESTCODE);
        }else if(v.getId() == R.id.switch_stop){
            initBusLineRecyclerAdapter.switchStop();
            //判断线路详情是否为空
            currentStop = stops.get(initBusLineRecyclerAdapter.getStopPosition());
            if(currentStop.getLines() == null || currentStop.getLines().size() == 0){
                params.put("buses", "1");
                mainPresenter.searchLinesByStationId(currentStop.getId(), params);
            }else{
                currentStop = initBusLineRecyclerAdapter.getCurrentStop();
                updateWidget(false);
            }
            initBusLineRecyclerAdapter.notifyDataSetChanged();
        } else if (v.getId() == R.id.btn_reminder) {
            String remindComeBusKey = Constants.SharedRemindKey.COME_BUS_REMIND;
            String remindGetOffBusKey = Constants.SharedRemindKey.GET_OFF_REMIND;
            //判断当前用户是否设置上车提醒或者下车提醒
            if (mSharedPreferences.getBoolean(remindGetOffBusKey, false)
                    || mSharedPreferences.getBoolean(remindComeBusKey, false)) {
                Intent intent = new Intent(getActivity(), ReminderSettingActivity.class);
                intent.putExtra(Constants.IntentKey.REMIND_STOP, currentStop.toString());
                startActivity(intent);
            } else {
                BusLineListDialogFragment busLineListDialogFragment =
                        BusLineListDialogFragment.newInstance(initBusLineRecyclerAdapter.getCurrentStop(),chooiceLines);
                busLineListDialogFragment.show(getActivity().getSupportFragmentManager(),"dialog");
            }
        }
    }

    public void updateWidget(boolean isSearch) {
        locationNameTextView.setText(currentStop.getName());
        if(isSearch){
            topSearchEdit.setText(currentStop.getName());
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onLongClick(View v) {
        Intent intent = new Intent(getActivity(), TestButtonActivity.class);
        startActivity(intent);
        return false;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
