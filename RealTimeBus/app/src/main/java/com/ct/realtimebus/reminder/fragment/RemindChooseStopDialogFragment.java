package com.ct.realtimebus.reminder.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.reminder.adapter.RemindChooseStopAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/16.
 */
public class RemindChooseStopDialogFragment extends DialogFragment {
    private static final String TAG = "BusLineListDialogFragment";

    private static final String STOPS = "stops";

    private RecyclerView recyclerViewStop;
    private RemindChooseStopAdapter remindChooseStopAdapter;
    private OnChooseStopListener onChooseStopListener;
    private ArrayList<BeanStop> stops;


    public RemindChooseStopDialogFragment() {
    }

    public static RemindChooseStopDialogFragment newInstance(ArrayList<BeanStop> stops) {
        RemindChooseStopDialogFragment remindChooseStopDialogFragment = new RemindChooseStopDialogFragment();
        Bundle bundle = new Bundle();
        remindChooseStopDialogFragment.setArguments(bundle);
        bundle.putSerializable(STOPS, stops);
        return remindChooseStopDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stop_list_dialog, container, false);
        recyclerViewStop = (RecyclerView) view.findViewById(R.id.recycler_stop);
        remindChooseStopAdapter = new RemindChooseStopAdapter(getActivity(), stops);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewStop.setLayoutManager(linearLayoutManager);
        recyclerViewStop.setAdapter(remindChooseStopAdapter);
        remindChooseStopAdapter.setItemClickListener(new RemindChooseStopAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                onChooseStopListener.chooseStop(stops.get(position), position);
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.stops = (ArrayList<BeanStop>) getArguments().getSerializable(STOPS);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void setOnChooseStopListener(OnChooseStopListener onChooseStopListener) {
        this.onChooseStopListener = onChooseStopListener;
    }

    public interface OnChooseStopListener {
        void chooseStop(BeanStop stop, int position);
    }
}
