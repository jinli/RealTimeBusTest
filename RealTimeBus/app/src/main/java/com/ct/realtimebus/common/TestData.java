package com.ct.realtimebus.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanLocation;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.test.MyTestSingle;
import com.ct.realtimebus.test.TestButtonActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/8.
 */
public class TestData {

    public static void startBusMove(Context context) throws JSONException {
        SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_REMIND, 0);
        MyTestSingle mts = MyTestSingle.getInstance();
        BeanLine line = mts.getLine();
        BeanStop currentStop = mts.getCurrentStop();
        ArrayList<BeanLocation> paths = line.getPaths();
        ArrayList<BeanStop> stops = line.getAllStops();

        if(mts.getNextStop() == null){
            mts.setNextStop(stops.get(1));
            mts.getNearBus().setStopId(stops.get(1).getId());
            mts.getNearBus().setLatitude(stops.get(1).getLatitude());
            mts.getNearBus().setLongitude(stops.get(1).getLongitude());
        }

        if (mts.getBusCurrentPosition() < paths.size() - 1) {
            mts.setBusCurrentPosition(mts.getBusCurrentPosition() + 1);
            BeanLocation path = paths.get(mts.getBusCurrentPosition());
            if(mts.getNearBus() != null){
                mts.getNearBus().setLatitude(path.getLat());
                mts.getNearBus().setLongitude(path.getLng());
            }
        }
        if(mts.getNearBus() != null) {
            for (int i = 0; i < stops.size(); i++) {
                BeanStop stop = stops.get(i);
                double buslat = mts.getNearBus().getLatitude();
                double buslng = mts.getNearBus().getLongitude();
                double stoplat = stop.getLatitude();
                double stoplng = stop.getLongitude();
//                Log.v("=========kaka===", mts.getNextStop() + "=======");
                if (buslat == stoplat && buslng == stoplng) {
                    if (i < stops.size() - 1) {
                        if (!(stop.getId() >= mts.getTerminal().getId())) {
                            BeanStop nextStop = stops.get(i + 1);
                            mts.getNearBus().setStopId(nextStop.getId());
                            mts.setNextStop(nextStop);
                            double nearBuslat = mts.getNearBus().getLatitude();
                            double nearBuslng = mts.getNearBus().getLongitude();
                            double nextStoplat = mts.getNextStop().getLatitude();
                            double nextStoplng = mts.getNextStop().getLongitude();
                            LatLng busll = new LatLng(nearBuslat, nearBuslng);
                            LatLng nextStopll = new LatLng(nextStoplat, nextStoplng);
                            double distance = AMapUtils.calculateLineDistance(busll, nextStopll);
                            mts.setTotleDistance(distance);
                        } else {

                        }
                    }
                    break;
                }
            }
        }

        if(mts.getNearBus() != null) {
            if (mts.getNextStop() != null) {
                //更新车辆距离
                double buslat = mts.getNearBus().getLatitude();
                double buslng = mts.getNearBus().getLongitude();
                Log.v("=========kaka===",buslat+"======="+buslng+"==========="+mts.getNearBus().getStopId());
                double nextStoplat = mts.getNextStop().getLatitude();
                double nextStoplng = mts.getNextStop().getLongitude();
                LatLng busll = new LatLng(buslat, buslng);
                LatLng nextStopll = new LatLng(nextStoplat, nextStoplng);
                double distance = AMapUtils.calculateLineDistance(busll, nextStopll);
                mts.getNearBus().setDistance(distance);
//            Log.v("=========kaka===",mts.getNextStop().getName()+"======="+distance);
            }
        }

        //上车提醒
        if (sp.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {

        }
        if(mts.getNearBus() != null) {
            //下车提醒
            if (sp.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)) {
                BeanStop terminal = new Gson().fromJson(sp.getString(Constants.SharedRemindKey.TERMINAL, ""), BeanStop.class);
                mts.setTerminal(terminal);
                if (mts.getNearBus().getLatitude() == terminal.getLatitude() &&
                        mts.getNearBus().getLongitude() == terminal.getLongitude()) {
                    mts.getNearBus().setSequence(1);
                }
            }
        }
    }

    public static void getTestData(Context context, String fileName, ImplDataResultListener dataResultListener) {
        JSONObject json = getAssetsJson(context, fileName);
        try {
            dataResultListener.onSuccess(json.getString("result"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getAssetsJson(Context context, String fileName) {
        JSONObject jsonObject = null;
        StringBuffer stringBuffer = new StringBuffer();
        AssetManager assetManager = context.getAssets();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
            String t = "";
            while (null != (t = bufferedReader.readLine())) {
                stringBuffer.append(t);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            jsonObject = new JSONObject(stringBuffer.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static boolean isOpenDemoData(Context context) {
        SharedPreferences sp = context.getSharedPreferences(TestButtonActivity.TEST_SETTING, 0);
        return sp.getBoolean(TestButtonActivity.DEMO_DATA, false);
    }
}
