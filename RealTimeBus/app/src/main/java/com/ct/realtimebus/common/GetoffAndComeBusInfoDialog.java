package com.ct.realtimebus.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ct.realtimebus.R;

/**
 * Created by zhangjinli on 16/9/6.
 */
public class GetoffAndComeBusInfoDialog extends Activity implements View.OnClickListener{
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fad_in, R.anim.fad_out);
        setContentView(R.layout.dialog_remind);
        intent = getIntent();
        Bundle bundle = intent.getExtras();
        Button button = (Button)findViewById(R.id.btn_close);
        button.setOnClickListener(this);
        TextView tvRemindContant = (TextView)findViewById(R.id.tv_remind_contant);
        if(bundle != null){
            String message = bundle.getString("msg");
            tvRemindContant.setText(message);
        }
    }

    @Override
    public void onClick(View v) {
        finish();
        overridePendingTransition(R.anim.fad_in, R.anim.fad_out);
    }
}
