package com.ct.realtimebus.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/1.
 */
public class SearchStopRecyclerAdapter extends RecyclerView.Adapter<SearchStopRecyclerAdapter.ItemViewHolder>{

    private Context context;

    private ArrayList<BeanStop> data;

    private LayoutInflater mLayoutInflater;

    private RecyclerItemClickListener mRecyclerItemClickListener;

    public SearchStopRecyclerAdapter(Context context, ArrayList<BeanStop> data) {
        this.context = context;
        this.data = data;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewHolder itemViewHolder = new ItemViewHolder(mLayoutInflater.inflate(R.layout.item_bus_station_list,parent,false));
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.tvBusStationName.setText(data.get(position).getName());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerItemClickListener.onRecyclerItemClick(position);
            }
        });
    }

    public void updateData(ArrayList<BeanStop> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        public View view;
        public TextView tvBusStationName;
        public ItemViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.tvBusStationName = (TextView)itemView.findViewById(R.id.tv_bus_station_name);
        }
    }

    public void setmRecyclerItemClickListener(RecyclerItemClickListener mRecyclerItemClickListener) {
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;
    }

    public interface RecyclerItemClickListener {
        void onRecyclerItemClick(int position);
    }
}
