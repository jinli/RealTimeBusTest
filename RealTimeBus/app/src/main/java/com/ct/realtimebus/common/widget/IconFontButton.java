package com.ct.realtimebus.common.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.ct.realtimebus.common.Constants;

/**
 * Created by zhangjinli on 16/8/1.
 */
public class IconFontButton extends Button {

    private Typeface typeface;

    public IconFontButton(Context context) {
        super(context);
        typeface = Typeface.createFromAsset(context.getAssets(), Constants.TTF_URI);
        setTypeface(typeface);
    }

    public IconFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeface = Typeface.createFromAsset(context.getAssets(), Constants.TTF_URI);
        setTypeface(typeface);
    }

    public IconFontButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        typeface = Typeface.createFromAsset(context.getAssets(), Constants.TTF_URI);
        setTypeface(typeface);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IconFontButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        typeface = Typeface.createFromAsset(context.getAssets(), Constants.TTF_URI);
        setTypeface(typeface);
    }
}
