package com.ct.realtimebus.mvp.model.inf;

import com.ct.realtimebus.bean.HistoryBean;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/29.
 */
public interface IHistoryModel {

    /**
     * 查询历史纪录 根据时间排序
     * @return
     */
    ArrayList<HistoryBean> queryHistoryByDateDesc();

}
