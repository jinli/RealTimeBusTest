package com.ct.realtimebus.lineinfo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.lineinfo.custom.TimeLineItemView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/5.
 */
public class BusTimeLineAdapter extends RecyclerView.Adapter<BusTimeLineAdapter.ItemViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private ArrayList<BeanStop> stops;
    private ArrayList<BeanBus> buses;
    private BeanStop currentStop;
    private int currentStopPosition;
    private int currentLineId;

    public BusTimeLineAdapter(Context mContext, int currentLineId, ArrayList<BeanStop> stops,ArrayList<BeanBus> buses, BeanStop currentStop) {
        this.mContext = mContext;
        this.buses = buses;
        this.stops = stops;
        this.currentStop = currentStop;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.currentLineId = currentLineId;
        for (int i = 0; i < stops.size(); i++) {
            if (stops.get(i).getName().equals(currentStop.getName())) {
                currentStopPosition = i;
                break;
            }
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_line_info, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
            BeanStop stop = stops.get(position);
            holder.timeLineItem.setStopDetail(stop);
            holder.timeLineItem.setBuses(buses);
            holder.timeLineItem.setCurrentStop(currentStop);
            holder.timeLineItem.setPosition(position);
            holder.timeLineItem.setCurrentStopPosition(currentStopPosition);
            holder.timeLineItem.setCurrentLineId(currentLineId);
            //设置终点起点和途径车站
            if (position == 0) {
                holder.timeLineItem.setIsStartStop(true);
            } else if (position == stops.size() - 1) {
                holder.timeLineItem.setIsTerminal(true);
            } else {
                holder.timeLineItem.setIsTerminal(false);
                holder.timeLineItem.setIsStartStop(false);
            }
    }

    public void updateData(ArrayList<BeanStop> stops) {
        this.stops = stops;
        for (int i = 0; i < stops.size(); i++) {
            if (stops.get(i).getName().equals(currentStop.getName())) {
                currentStopPosition = i;
                break;
            }
        }
    }

    public int setBuses(ArrayList<BeanBus> buses){
        int position = -1;
        this.buses = buses;
        for (int i = 0; i < stops.size(); i++) {
            if (buses != null && buses.size() > 0 && buses.get(0).getStopId() == stops.get(i).getId()) {
                position = i;
                break;
            }
        }
        return position;
    }

    public void updateCurrentLineId(int currentLineId){
        this.currentLineId = currentLineId;
    }


    @Override
    public int getItemCount() {
        return stops != null ? stops.size() : 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TimeLineItemView timeLineItem;

        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            timeLineItem = (TimeLineItemView) view.findViewById(R.id.time_line_item);
        }
    }
}
