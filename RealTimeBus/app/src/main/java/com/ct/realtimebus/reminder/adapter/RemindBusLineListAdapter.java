package com.ct.realtimebus.reminder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanChooiceLine;
import com.ct.realtimebus.bean.BeanLine;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/15.
 */
public class RemindBusLineListAdapter extends RecyclerView.Adapter<RemindBusLineListAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<BeanChooiceLine> lines;

    private LayoutInflater layoutInflater;

    private ItemClickListener itemClickListener;

    private int subPosition;

    public RemindBusLineListAdapter(Context context, ArrayList<BeanChooiceLine> lines) {
        this.mContext = context;
        this.lines = lines;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_remind_bus_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        holder.lineNameView.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        holder.tvLine.setText(lines.get(position).getLineName());

        LayoutInflater childInflater = LayoutInflater.from(mContext);
        ArrayList<BeanLine> chooiceLines = lines.get(position).getLines();
        for(int i = 0; i < chooiceLines.size(); i ++){
            subPosition = i;
            BeanLine childLine = chooiceLines.get(i);
            final View childView = childInflater.inflate(R.layout.item_remind_bus_list_child, (ViewGroup) holder.view, false);
            TextView terminal = (TextView) childView.findViewById(R.id.tv_terminal);
            terminal.setText(childLine.getTerminal());
            ((ViewGroup) holder.view).addView(childView);
            childView.setTag(subPosition);
            childView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position,(int)childView.getTag());
                }
            });
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return lines != null ? lines.size() : 0;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLine;
        public LinearLayout lineNameView;
        public View view;

        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvLine = (TextView) itemView.findViewById(R.id.tv_line);
            lineNameView = (LinearLayout)itemView.findViewById(R.id.line_name_view);
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position,int subPosition);
    }
}
