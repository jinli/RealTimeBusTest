package com.ct.realtimebus.mvp.presenter;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanChooiceLine;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.VolleyHelper;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.view.MainView;
import com.ct.realtimebus.mvp.model.LineModel;
import com.ct.realtimebus.mvp.model.StopModel;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.mvp.model.inf.IStopModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangjinli on 16/8/2.
 */
public class MainPresenter {

    private IStopModel stopModel;
    private ILineModel lineModel;
    private MainView mainView;
    private Context mContext;

    public MainPresenter(Context context, MainView mainView) {
        this.stopModel = new StopModel();
        this.lineModel = new LineModel();
        this.mContext = context;
        this.mainView = mainView;
        this.stopModel.setContext(mContext);
        this.lineModel.setContext(mContext);
    }

    public void getBusStationList(HashMap<String, String> params) {
        stopModel.getNearbyStops(params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String resultData) {
                try {
                    JSONObject result = new JSONObject(resultData);
                    Type stopType = new TypeToken<ArrayList<BeanStop>>() {
                    }.getType();
                    Type lineType = new TypeToken<ArrayList<BeanLine>>() {
                    }.getType();
                    Gson gson = new Gson();
                    ArrayList<BeanStop> stops = gson.fromJson(result.getJSONArray("stops").toString(), stopType);
                    ArrayList<BeanLine> lines = gson.fromJson(result.getJSONArray("lines").toString(), lineType);
                    HashMap<String,ArrayList<Integer>> lineIdMap = new HashMap<>();

                    //整理数据
                    for (BeanStop stop : stops) {
                        ArrayList<BeanLine> stopLine = new ArrayList<>();
                        for (Integer lineId : stop.getRefs()) {
                            for (BeanLine line : lines) {
                                if (lineId == line.getId()) {
                                    ArrayList<BeanBus> buses = new ArrayList<>();
                                    for (BeanBus bus : stop.getBuses()) {
                                        if (bus.getLineId() == lineId) {
                                            buses.add(bus);
                                        }
                                    }
                                    line.setBuses(buses);
                                    stopLine.add(line);
                                }
                            }
                        }
                        stop.setLines(stopLine);
                    }

                    ArrayList<BeanChooiceLine> chooiceLines = new ArrayList<>();

                    for(BeanLine line : lines){
                        //整理线路以用来区分是环线还是双向
                        if(!lineIdMap.containsKey(line.getLine())){
                            ArrayList<Integer> ids = new ArrayList<>();
                            ids.add(line.getId());
                            lineIdMap.put(line.getLine(),ids);
                        }else{
                            lineIdMap.get(line.getLine()).add(line.getId());
                        }

                        boolean isHas = false;
                        for(BeanChooiceLine beanChooiceLine : chooiceLines){
                            if(line.getLine().equals(beanChooiceLine.getLineName())){
                                beanChooiceLine.getLines().add(line);
                                isHas = true;
                            }
                        }
                        if(!isHas){
                            BeanChooiceLine beanChooiceLine = new BeanChooiceLine();
                            beanChooiceLine.getLines().add(line);
                            beanChooiceLine.setLineName(line.getLine());
                            chooiceLines.add(beanChooiceLine);
                        }
                    }
                    mainView.initBusStationList(stops,lineIdMap,chooiceLines);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void searchLinesByStationId(int stationId, HashMap<String, String> params) {
        lineModel.searchLinesByStopId(stationId, params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String resultData) {
                Type type = new TypeToken<ArrayList<BeanLine>>() {
                }.getType();
                Gson gson = new Gson();
                ArrayList<BeanLine> lines = gson.fromJson(resultData, type);
                mainView.updateBusRecycler(lines);
            }
        });
    }
}
