package com.ct.realtimebus.common;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanStop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/9/21.
 */
public class BusHelper {
    /**
     * 获取车辆集合中 距离站点最近的车辆
     * @param buses
     * @param stop
     * @return
     * @throws JSONException
     */
    public static BeanBus getNearBus(ArrayList<BeanBus> buses, BeanStop stop){
        BeanBus nearBus = null;
        for (BeanBus bus : buses) {
            LatLng busll = new LatLng(bus.getLatitude(), bus.getLongitude());
            LatLng stopll = new LatLng(stop.getLatitude(), stop.getLongitude());
            float discent = AMapUtils.calculateLineDistance(busll, stopll);
            if (nearBus == null) {
                nearBus = bus;
            } else {
                LatLng nearBusll = new LatLng(nearBus.getLatitude(), nearBus.getLongitude());
                float nearDiscent = AMapUtils.calculateLineDistance(nearBusll, stopll);
                if (nearDiscent > discent) {
                    nearBus = bus;
                }
            }
        }
        return nearBus;
    }
}
