package com.ct.realtimebus.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.HistoryBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by zhangjinli on 16/8/29.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ItemViewHolder>{

    private Context mContext;
    private HistoryBean data[];
    private LayoutInflater layoutInflater;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public HistoryAdapter(Context context, HistoryBean[] data) {
        this.mContext = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_recycler_history,parent,false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.tvLineName.setText(data[position].getLineName());
        holder.tvFrom.setText(data[position].getFromStopName());
        holder.tvTo.setText(data[position].getToStopName());
        try {
            holder.tvDate.setText(simpleDateFormat.format(simpleDateFormat.parse(data[position].getCreateDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void update(HistoryBean[] data){
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data != null ? data.length : 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        public View view;
        public TextView tvLineName;
        public TextView tvFrom;
        public TextView tvTo;
        public TextView tvDate;
        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvLineName = (TextView)itemView.findViewById(R.id.tv_line_name);
            tvFrom = (TextView)itemView.findViewById(R.id.tv_from);
            tvTo = (TextView)itemView.findViewById(R.id.tv_to);
            tvDate = (TextView)itemView.findViewById(R.id.tv_date);
        }
    }
}
