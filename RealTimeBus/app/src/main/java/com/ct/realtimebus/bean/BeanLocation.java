package com.ct.realtimebus.bean;

import java.io.Serializable;

/**
 * Created by zhangjinli on 16/9/28.
 */
public class BeanLocation implements Serializable{
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
