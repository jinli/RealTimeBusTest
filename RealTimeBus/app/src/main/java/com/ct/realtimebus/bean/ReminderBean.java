package com.ct.realtimebus.bean;

/**
 * Created by zhangjinli on 16/8/12.
 */
public class ReminderBean {
    private int id;
    private int destinationId;//目的地Id
    private String destinationText;//目的地名称
    private int surplusStops;//剩余站数
    private String estimatedTime;//预计时间
    private int getOffRemind;//下车提醒开关 0-开 1-关
    private int comeBusRemind;//来车提醒开关 0-开 1-关

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationText() {
        return destinationText;
    }

    public void setDestinationText(String destinationText) { this.destinationText = destinationText; }

    public int getSurplusStops() {
        return surplusStops;
    }

    public void setSurplusStops(int surplusStops) {
        this.surplusStops = surplusStops;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getGetOffRemind() {
        return getOffRemind;
    }

    public void setGetOffRemind(int getOffRemind) {
        this.getOffRemind = getOffRemind;
    }

    public int getComeBusRemind() {
        return comeBusRemind;
    }

    public void setComeBusRemind(int comeBusRemind) {
        this.comeBusRemind = comeBusRemind;
    }
}
