package com.ct.realtimebus.mvp.view;

import com.ct.realtimebus.bean.BeanChooiceLine;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by zhangjinli on 16/8/2.
 */
public interface MainView {

    void initBusStationList(ArrayList<BeanStop> stops,HashMap<String,ArrayList<Integer>> lineIdMap,ArrayList<BeanChooiceLine> chooiceLines);

    void updateBusRecycler(ArrayList<BeanLine> lines);

}
