package com.ct.realtimebus.splash.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Bundle;
import android.os.Message;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.BaseActivity;
import com.ct.realtimebus.common.MyApplication;
import com.ct.realtimebus.common.PermissionHelper;
import com.ct.realtimebus.main.LruHistory;
import com.ct.realtimebus.main.activity.MainActivity;
import com.ct.realtimebus.service.HistoryService;

/**
 * Splash activity
 *
 * Create by zhangjinli on 2016/07/28
 */
public class SplashActivity extends Activity {
    public static final String QUERY_RESUTL = "com.ct.realtimebus.main.service.action.QUERY_RESULT";
    private static final String TAG = "SplashActivity";

    private Handler handler;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction() == QUERY_RESUTL){
                handler.sendEmptyMessage(0);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MyApplication myApplication = MyApplication.getInstance();
        myApplication.setLruHistory(new LruHistory(10, getApplicationContext()));
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(intent);
                finish();

            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(QUERY_RESUTL);
        registerReceiver(receiver, intentFilter);
        HistoryService.startQueryHistory(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
