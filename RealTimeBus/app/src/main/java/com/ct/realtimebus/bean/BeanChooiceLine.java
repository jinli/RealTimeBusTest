package com.ct.realtimebus.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/9/29.
 */
public class BeanChooiceLine implements Serializable{
    private String lineName;
    private ArrayList<BeanLine> lines = new ArrayList<>();

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public ArrayList<BeanLine> getLines() {
        return lines;
    }

    public void setLines(ArrayList<BeanLine> lines) {
        this.lines = lines;
    }
}
