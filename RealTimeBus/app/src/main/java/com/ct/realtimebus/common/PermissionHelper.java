package com.ct.realtimebus.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;


/**
 * Created by zhangjinli on 16/9/8.
 */
public class PermissionHelper {
    public static final int REQUEST_ALL = 100;
    public static final int ACCESS_LOCATION_REQUEST_CODE = 101;
    public static final int STORAGE_REQUEST_CODE = 102;

    public static final String PERMISSION_COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final String PERMISSION_STORAGE  = Manifest.permission.READ_EXTERNAL_STORAGE;

    public static final String [] permissions = {PERMISSION_COARSE_LOCATION,PERMISSION_STORAGE};
    /**
     * 对某一权限进行检查和申请
     * @param context
     * @param permission
     */
    public static boolean checkPermission(Context context,String permission){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * 检查并申请所有需要的权限
     * @param context
     */
    public static void checkAllPermission(Context context){
        ArrayList<String> p = new ArrayList<>();
        for(String permission : permissions){
            if(!checkPermission(context,permission)){
                p.add(permission);
            }
        }
        if(p != null && p.size() > 0){
            String[] pp = p.toArray(new String[p.size()]);
            requestPermissions(context,pp);
        }

    }

    public static void requestPermissions(Context context,String[] permissions){
        ActivityCompat.requestPermissions((Activity)context, permissions,REQUEST_ALL);
    }

    public static void requestPermission(Context context,int code,String permission){
        ActivityCompat.requestPermissions((Activity)context, new String[]{permission},
                code);
    }


}
