package com.ct.realtimebus.test;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanLocation;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BusHelper;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.TestData;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class TestService extends IntentService {

    public static final String ACTION_FOO = "com.ct.realtimebus.test.action.FOO";
    private SharedPreferences sp;

    public TestService() {
        super("TestService");
    }

    public static void move(Context context) {
        Intent intent = new Intent(context, TestService.class);
        intent.setAction(ACTION_FOO);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                handleActionFoo();
            }
        }
    }

    private void handleActionFoo() {
        sp = getApplicationContext().getSharedPreferences(Constants.SHARED_REMIND,0);
        MyTestSingle mts = MyTestSingle.getInstance();
        try {
            Gson gson = new Gson();
            BeanLine line = gson.fromJson(sp.getString(Constants.SharedRemindKey.REMIND_LINE,""), BeanLine.class);
            BeanStop currentStop = gson.fromJson(sp.getString(Constants.SharedRemindKey.CURRENT_STOP,""), BeanStop.class);
            if(sp.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND,false)){
                ArrayList<BeanLocation> paths = line.getPaths();
                mts.setLine(line);
                mts.setCurrentStop(currentStop);
                BeanBus nearBus = BusHelper.getNearBus(line.getBuses(), currentStop);
                mts.setNearBus(nearBus);
                ArrayList<BeanStop> stops = line.getAllStops();
                int nearBusStopId = nearBus.getStopId();
                for(int i = 0; i < stops.size(); i ++){
                    int stopId = stops.get(i).getId();
                    if(nearBusStopId == stopId){
                        mts.getNearBus().setLatitude(stops.get(i - 2).getLatitude());
                        mts.getNearBus().setLatitude(stops.get(i - 2).getLongitude());
                        break;
                    }
                }
                for(int i = 0; i < paths.size(); i ++){
                    BeanLocation path = paths.get(i);
                    if(path.getLat() == mts.getNearBus().getLatitude() && path.getLng() == mts.getNearBus().getLongitude()){
                        mts.setBusCurrentPosition(i);
                        break;
                    }
                }
            }

            if(sp.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)){
                BeanStop terminal = gson.fromJson(sp.getString(Constants.SharedRemindKey.TERMINAL, ""), BeanStop.class);
                mts.setLine(line);
                mts.setCurrentStop(currentStop);
                mts.setTerminal(terminal);
                BeanBus nearBus = BusHelper.getNearBus(line.getBuses(),terminal);
                ArrayList<BeanLocation> paths = line.getPaths();
                mts.setNearBus(nearBus);
                ArrayList<BeanStop> stops = line.getStops();
                if(!sp.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND,false)){
                    mts.getNearBus().setStopId(currentStop.getId());
                }
                for(int i = 0; i < stops.size(); i ++){
                    if(stops.get(i).getId() == nearBus.getStopId()){
                        mts.getNearBus().setLatitude(stops.get(i - 2).getLatitude());
                        mts.getNearBus().setLongitude(stops.get(i - 2).getLongitude());
                        break;
                    }
                }
                for(int i = 0; i < paths.size(); i ++){
                    BeanLocation path = paths.get(i);
                    if(path.getLat() == mts.getNearBus().getLatitude() && path.getLng() == mts.getNearBus().getLongitude()){
                        mts.setBusCurrentPosition(i);
                        break;
                    }
                }
            }

            while(sp.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND,false) || sp.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND,false)){
                try {
                    new Thread().sleep(MyTestSingle.period);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                TestData.startBusMove(getApplicationContext());
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

}
