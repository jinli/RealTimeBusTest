package com.ct.realtimebus.mvp.presenter;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.VolleyHelper;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.model.inf.IStopModel;
import com.ct.realtimebus.mvp.model.StopModel;
import com.ct.realtimebus.mvp.view.SearchView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public class SearchPresenter {

    private Context mContext;
    private SearchView searchView;
    private IStopModel stopModel;

    public SearchPresenter(Context mContext, SearchView searchView) {
        this.mContext = mContext;
        this.searchView = searchView;
        this.stopModel = new StopModel();
        this.stopModel.setContext(mContext);
    }

    public void searchStopsByKeywords(HashMap<String, String> params) {
        stopModel.searchStopsByKeywords(params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String resultData) {
                Type type = new TypeToken<ArrayList<BeanStop>>() {
                }.getType();
                Gson gson = new Gson();
                ArrayList<BeanStop> stops = gson.fromJson(resultData, type);
                searchView.updateSearchStopList(stops);
            }
        });
    }

    public void getStopsByLocation(HashMap<String, String> params) {
        stopModel.getNearbyStops(params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String resultData) {
                try {
                    JSONObject result = new JSONObject(resultData);
                    Type type = new TypeToken<ArrayList<BeanStop>>(){}.getType();
                    Gson gson = new Gson();
                    ArrayList<BeanStop> stops = gson.fromJson(result.getJSONArray("stops").toString(),type);
                    searchView.updateMap(stops);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String msg, Exception e) {
                super.onFailure(msg, e);
                searchView.updateMap(null);
            }
        });
    }
}
