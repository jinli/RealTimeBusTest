package com.ct.realtimebus.common.dbmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ct.realtimebus.common.DatabaseHelper;
import com.ct.realtimebus.bean.HistoryBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangjinli on 16/8/29.
 */
public class HistoryDBManager {

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_STOP_ID = "stopId";
    public static final String COLUMN_STOP_NAME = "stopName";
    public static final String COLUMN_LINE_ID = "lineId";
    public static final String COLUMN_LINE_NAME = "lineName";
    public static final String COLUMN_CREATE_DATE = "createDate";
    public static final String COLUMN_FROM_STOP_NAME = "fromStopName";
    public static final String COLUMN_TO_STOP_NAME = "toStopName";
    public static final String COLUMN_SUBSCRIBE_OR_READ = "subscribeOrRead";
    public static final String COLUMN_SOR_TIMES = "sorTimes";

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;

    public HistoryDBManager(Context context) {
        databaseHelper = new DatabaseHelper(context);
        db = databaseHelper.getWritableDatabase();
    }

    /**
     * 添加记录
     * 可以添加一条，也可以添加多条
     * @param historyBeanList
     */
    public void add(List<HistoryBean> historyBeanList) {
        String sql = "INSERT INTO " + DatabaseHelper.TABLE_HISTORY + " VALUES(null,?,?,?,?,?,?,?,?,?)";
        /**开启事务处理 保证数据完整性**/
        db.beginTransaction();
        try {
            for (HistoryBean historyBean : historyBeanList) {
                db.execSQL(sql,
                        new Object[]{
                                historyBean.getStopId(),
                                historyBean.getStopName(),
                                historyBean.getLineId(),
                                historyBean.getLineName(),
                                historyBean.getCreateDate(),
                                historyBean.getFromStopName(),
                                historyBean.getToStopName(),
                                historyBean.getSubscribeOrRead(),
                                historyBean.getSorTimes()});
            }
            db.setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            db.endTransaction();
        }
    }

    /**
     * 修改记录
     *
     * @param historyBean
     */
    public void update(HistoryBean historyBean) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID,historyBean.getId());
        contentValues.put(COLUMN_STOP_ID,historyBean.getStopId());
        contentValues.put(COLUMN_STOP_NAME,historyBean.getStopName());
        contentValues.put(COLUMN_LINE_ID,historyBean.getLineId());
        contentValues.put(COLUMN_LINE_NAME,historyBean.getLineName());
        contentValues.put(COLUMN_CREATE_DATE,historyBean.getCreateDate());
        contentValues.put(COLUMN_FROM_STOP_NAME,historyBean.getFromStopName());
        contentValues.put(COLUMN_TO_STOP_NAME,historyBean.getToStopName());
        contentValues.put(COLUMN_SUBSCRIBE_OR_READ,historyBean.getSubscribeOrRead());
        contentValues.put(COLUMN_SOR_TIMES,historyBean.getSorTimes());
        db.update(DatabaseHelper.TABLE_HISTORY, contentValues, "id = ?", new String[]{historyBean.getId() + ""});
    }

    /**
     * 删除记录
     *
     * @param id
     */
    public void delete(Integer id) {
        db.delete(DatabaseHelper.TABLE_HISTORY, "id = ?", new String[]{id + ""});
    }

    public void deleteAll(){
        db.execSQL("delete from "+DatabaseHelper.TABLE_HISTORY + " where 1 = 1");
    }

    /**
     * 查询全部历史纪录 根据时间排序
     *
     * @return
     */
    public ArrayList<HistoryBean> queryAll() {
        String sql = "SELECT * from " + DatabaseHelper.TABLE_HISTORY;
        ArrayList<HistoryBean> list = new ArrayList<>();
        Cursor cursor = db.rawQuery(sql,null);
        while (cursor.moveToNext()){
            HistoryBean historyBean = new HistoryBean();
            historyBean.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
            historyBean.setStopId(cursor.getInt(cursor.getColumnIndex(COLUMN_STOP_ID)));
            historyBean.setStopName(cursor.getString(cursor.getColumnIndex(COLUMN_STOP_NAME)));
            historyBean.setLineId(cursor.getInt(cursor.getColumnIndex(COLUMN_LINE_ID)));
            historyBean.setLineName(cursor.getString(cursor.getColumnIndex(COLUMN_LINE_NAME)));
            historyBean.setCreateDate(cursor.getString(cursor.getColumnIndex(COLUMN_CREATE_DATE)));
            historyBean.setFromStopName(cursor.getString(cursor.getColumnIndex(COLUMN_FROM_STOP_NAME)));
            historyBean.setToStopName(cursor.getString(cursor.getColumnIndex(COLUMN_TO_STOP_NAME)));
            historyBean.setSubscribeOrRead(cursor.getInt(cursor.getColumnIndex(COLUMN_SUBSCRIBE_OR_READ)));
            historyBean.setSorTimes(cursor.getInt(cursor.getColumnIndex(COLUMN_SOR_TIMES)));
            list.add(historyBean);
        }
        db.close();
        return list;
    }

    public Cursor queryTheCursor(String sql) {

        return null;
    }

    public Cursor cudHistory(String sql) {
        return null;
    }
}
