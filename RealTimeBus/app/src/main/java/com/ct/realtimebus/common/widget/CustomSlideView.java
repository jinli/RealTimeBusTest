package com.ct.realtimebus.common.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.RTBUtils;

/**
 * Created by zhangjinli on 16/8/23.
 */
public class CustomSlideView extends RelativeLayout implements View.OnTouchListener {
    private static final String TAG = "CustomSlideView";
    public static final int IS_TOP = 0;
    public static final int IS_CENTER = 1;
    public static final int IS_BOTTOM = 2;
    public static final int DIRECTION_UP = 0;
    public static final int DIRECTION_DOWN = 1;

    private View _customView;
    private LayoutInflater _layoutInflater;
    private RelativeLayout _layout;
    private RelativeLayout _slideBar;
    private Context _context;

    private SlideStatusListener slideStatusListener;

    /**
     * 三种状态下_layout的高度
     **/
    private int _verticalTop;
    private int _verticalCenter;
    private int _verticalBottom;

    private int slideBarHeight = 0;

    /**
     * 总高度不包含statuBar
     **/
    private int totalHeight;
    /**
     * 状态栏高度
     **/
    private int statuBarHeight;

    private int startY = 0;
    private int endY = 0;
    private int centerStartY = 0;
    /**
     * 用户触摸一瞬间的高度，用来判断slide的位置
     **/
    private int startHeight = 0;

    /**
     * 当前状态
     */
    private int nowStatu = 1;

    public CustomSlideView(Context context) {
        super(context);
        this._layoutInflater = LayoutInflater.from(context);
        this._customView = _layoutInflater.inflate(R.layout.layout_custom_slide_view, null);
        this._layout = (RelativeLayout) _customView.findViewById(R.id.layout);
        this._context = context;
        initCustomAttrs();
        addView(_customView);
    }

    public CustomSlideView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this._layoutInflater = LayoutInflater.from(context);
        this._customView = _layoutInflater.inflate(R.layout.layout_custom_slide_view, null);
        this._layout = (RelativeLayout) _customView.findViewById(R.id.layout);
        this._context = context;
        initCustomAttrs();
        addView(_customView);
    }

    public CustomSlideView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this._layoutInflater = LayoutInflater.from(context);
        this._customView = _layoutInflater.inflate(R.layout.layout_custom_slide_view, null);
        this._layout = (RelativeLayout) _customView.findViewById(R.id.layout);
        this._context = context;
        initCustomAttrs();
        addView(_customView);
    }

    /**
     * 初始化
     */
    private void initCustomAttrs() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        /**添加外部布局**/
        _customView.setLayoutParams(layoutParams);
        /**设置背景色**/
        _customView.setBackgroundColor(_context.getResources().getColor(R.color.colorWhite));
        /**初始化slideBar**/
        _slideBar = (RelativeLayout) _customView.findViewById(R.id.slide_bar);
        _slideBar.setOnTouchListener(this);
        /**状态栏高度**/
        statuBarHeight = RTBUtils.getStatuBarHeight(_context);
        /**计算Contant布局最大高度**/
        WindowManager windowManager = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
        totalHeight = windowManager.getDefaultDisplay().getHeight() - statuBarHeight;
        _verticalTop = totalHeight;
        _verticalCenter = totalHeight / 2;
        _verticalBottom = 0;
        /**初始化内容区域**/
        initContentLayout();
        setWillNotDraw(false);
        _layout.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    /**
     * 初始化内容区域id为layout的View部分
     */
    public void initContentLayout() {
        /**设置内容区域的初始高度**/
        ViewGroup.LayoutParams lp = _layout.getLayoutParams();
        lp.height = _verticalCenter;
        nowStatu = IS_CENTER;
        _layout.setLayoutParams(lp);
        /**禁止触摸事件向下层传递**/
        _layout.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void setContentView(View view) {
        _layout.addView(view);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.slide_bar) {
            final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _layout.getLayoutParams();
            int height = layoutParams.height;
            /**获得手指按下纵坐标**/
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                startY = (int) event.getRawY();
                startHeight = height;
                if (startHeight == _verticalCenter) {
                    centerStartY = startY + startHeight;
                }
            }
            /**判断作用于SlideBar的手指滑动事件**/
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                int moveY = (int) event.getRawY();
                /**判断手指是向上还是向下滑动**/
                if (startY > moveY) {
                    /**判断位置**/
                    if (startHeight == _verticalBottom) {
                        layoutParams.height = startY - moveY;
                    } else if (startHeight == _verticalCenter) {
                        layoutParams.height = centerStartY - moveY;
                    }
                } else {
                    if (startHeight == _verticalTop) {
                        layoutParams.height = totalHeight - moveY;
                    } else if (startHeight == _verticalCenter) {
                        layoutParams.height = centerStartY - moveY;
                    }
                }
                _layout.setLayoutParams(layoutParams);
            }

            /**获得手指离开时的纵坐标**/
            if (event.getAction() == MotionEvent.ACTION_UP) {
                endY = (int) event.getRawY();
                /**判断Slide位置并且滑动的距离大于屏幕的1/10，Slide变为中间状态，如果小于1/10回到底部**/
                if (startY - endY > 0) {
                    if (height > totalHeight / 10 * 2 && height < totalHeight / 10 * 7) {
                        animation(layoutParams, height, _verticalCenter);
                    } else if (height > totalHeight / 10 * 6) {
                        animation(layoutParams, height, _verticalTop);
                    } else if (height < totalHeight / 10 * 2) {
                        animation(layoutParams, height, _verticalBottom);
                    }
                } else {
                    if (height < totalHeight / 10 * 7 && height > totalHeight / 10 * 3) {
                        animation(layoutParams, height, _verticalCenter);
                    } else if (height < totalHeight / 10 * 3) {
                        animation(layoutParams, height, _verticalBottom);
                    } else if (height > totalHeight / 10 * 7) {
                        animation(layoutParams, height, _verticalTop);
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /**临时解决方案  获取slidebar的高度**/
        if (slideBarHeight == 0) {
            slideBarHeight = _slideBar.getHeight();
            totalHeight = totalHeight - slideBarHeight;
            _verticalTop = totalHeight;
            _verticalCenter = totalHeight / 2;

            ViewGroup.LayoutParams lp = _layout.getLayoutParams();
            lp.height = _verticalCenter;
            _layout.setLayoutParams(lp);
        }
    }

    /***
     * 用来改变高度的属性动画
     *
     * @param layoutParams
     * @param startHeight
     * @param endHeight
     */
    private void animation(final LinearLayout.LayoutParams layoutParams, int startHeight, int endHeight) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(startHeight, endHeight);
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                if(slideStatusListener != null){
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)_layout.getLayoutParams();
                    if(params.height == _verticalTop){
                        nowStatu = IS_TOP;
                        slideStatusListener.complateSlide(IS_TOP);
                    }else if(params.height == _verticalCenter){
                        nowStatu = IS_CENTER;
                        slideStatusListener.complateSlide(IS_CENTER);
                    }else if(params.height == _verticalBottom){
                        nowStatu = IS_BOTTOM;
                        slideStatusListener.complateSlide(IS_BOTTOM);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        valueAnimator.setDuration(300).start();
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int height = (int) animation.getAnimatedValue();
                layoutParams.height = height;
                _layout.setLayoutParams(layoutParams);
            }
        });

    }

    public void changeView(int statu){
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _layout.getLayoutParams();
        int viewHeight = layoutParams.height;
        switch (statu){
            case IS_TOP:
                animation(layoutParams,viewHeight,_verticalTop);
                nowStatu = statu;
                break;
            case IS_CENTER:
                animation(layoutParams,viewHeight,_verticalCenter);
                nowStatu = statu;
                break;
            case IS_BOTTOM:
                animation(layoutParams,viewHeight,_verticalBottom);
                nowStatu = statu;
                break;
        }
    }

    /**
     * 该方法负责关闭自定义View
     * 当View处于Top时，该View将位置变换到中部，当View处于中部时，该View再回到底部
     *
     * @close 当该参数为true时，不论View的位置都回到最底部
     */
    public void dismiss(boolean close) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _layout.getLayoutParams();
        int viewHeight = layoutParams.height;
        if (!close) {
            if (viewHeight == _verticalTop) {
                animation(layoutParams, layoutParams.height, _verticalCenter);
            } else if (viewHeight == _verticalCenter) {
                animation(layoutParams, layoutParams.height, _verticalBottom);
            }
        } else {
            animation(layoutParams, layoutParams.height, _verticalBottom);
        }
    }

    /**
     * 与关闭方法相反
     *
     * @param open
     */
    public void show(boolean open) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _layout.getLayoutParams();
        int viewHeight = layoutParams.height;
        if (!open) {
            if (viewHeight == _verticalBottom) {
                animation(layoutParams, layoutParams.height, _verticalCenter);
            } else if (viewHeight == _verticalCenter) {
                animation(layoutParams, layoutParams.height, _verticalTop);
            }
        } else {
            animation(layoutParams, layoutParams.height, _verticalTop);
        }
    }

    public void setSlideStatusListener(SlideStatusListener slideStatusListener) {
        this.slideStatusListener = slideStatusListener;
    }

    public int getSlideBarHeight(){
        return slideBarHeight;
    }

    public int getTotalHeight(){
        return totalHeight + slideBarHeight;
    }

    public interface SlideStatusListener{
        void complateSlide(int position);
    }

    public int getNowStatu() {
        return nowStatu;
    }
}
