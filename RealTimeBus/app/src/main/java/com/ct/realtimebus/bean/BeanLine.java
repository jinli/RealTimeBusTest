package com.ct.realtimebus.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 公交线路Bean
 * Created by zhangjinli on 16/8/2.
 */
public class BeanLine implements Serializable{

    private String basePrice;
    private int busSize;
    private ArrayList<BeanBus> buses;
    private String code;
    private int companyId;
    private String endTime;
    private boolean iccard;
    private int id;
    private double length;
    private String line;
    private String name;
    private String start;
    private String startSpell;
    private String startTime;
    private String terminal;
    private String terminalSpell;
    private String totalPrice;
    private String type;
    private String updated;
    private ArrayList<BeanStop> stops;
    private ArrayList<BeanLocation> paths;
    private double rate;
    private int at;
    private ArrayList<BeanStop> allStops;

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public int getBusSize() {
        return busSize;
    }

    public void setBusSize(int busSize) {
        this.busSize = busSize;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isIccard() {
        return iccard;
    }

    public void setIccard(boolean iccard) {
        this.iccard = iccard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStartSpell() {
        return startSpell;
    }

    public void setStartSpell(String startSpell) {
        this.startSpell = startSpell;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminalSpell() {
        return terminalSpell;
    }

    public void setTerminalSpell(String terminalSpell) {
        this.terminalSpell = terminalSpell;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public ArrayList<BeanBus> getBuses() {
        return buses;
    }

    public void setBuses(ArrayList<BeanBus> buses) {
        this.buses = buses;
    }

    public ArrayList<BeanStop> getStops() {
        return stops;
    }

    public void setStops(ArrayList<BeanStop> stops) {
        this.stops = stops;
    }

    public ArrayList<BeanLocation> getPaths() {
        return paths;
    }

    public void setPaths(ArrayList<BeanLocation> paths) {
        this.paths = paths;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getAt() {
        return at;
    }

    public void setAt(int at) {
        this.at = at;
    }

    public ArrayList<BeanStop> getAllStops() {
        return allStops;
    }

    public void setAllStops(ArrayList<BeanStop> allStops) {
        this.allStops = allStops;
    }
}
