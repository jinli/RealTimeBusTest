package com.ct.realtimebus.common;

import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.bean.HistoryBean;
import com.ct.realtimebus.main.LruHistory;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * 用于全局共享的单例模式
 * Created by zhangjinli on 16/8/3.
 */
public class MyApplication {
    private LruHistory lruHistory;
    private static MyApplication myApplication;

    private MyApplication() {}

    public static MyApplication getInstance(){
        if(myApplication == null){
            myApplication = new MyApplication();
        }
        return myApplication;
    }

    public void setLruHistory(LruHistory lruHistory) {
        this.lruHistory = lruHistory;
    }

    public LruHistory getLruHistory() {
        return lruHistory;
    }

    public LinkedHashMap<Integer,HistoryBean> getHistory(){
        return lruHistory.getCache();
    }

    public void addHistory(BeanStop stop,BeanLine line,int subscribeOrRead){
        HistoryBean historyBean = new HistoryBean();
            historyBean.setLineId(line.getId());
            historyBean.setLineName(line.getLine());
            historyBean.setStopId(stop.getId());
            historyBean.setStopName(stop.getName());
            historyBean.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            historyBean.setFromStopName(stop.getName());
            if(subscribeOrRead == LruHistory.SUBSCRIBE){
                historyBean.setToStopName(stop.getToStopName());
            }else{
                historyBean.setToStopName(line.getTerminal());
            }
            historyBean.setSubscribeOrRead(subscribeOrRead);
            historyBean.setSorTimes(1);
            if(lruHistory != null){
                lruHistory.add(historyBean);
            }


    }

    public static class MyApplicationHolder{
        public static final MyApplication myApplication = new MyApplication();
    }

}
