package com.ct.realtimebus.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BusHelper;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.TestData;
import com.ct.realtimebus.lineinfo.activity.LineInfoActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by zhangjinli on 16/7/29.
 */
public class InitBusLineRecyclerAdapter extends RecyclerView.Adapter<InitBusLineRecyclerAdapter.ItemViewHolder> {

    private Context context;
    private LayoutInflater mLayoutInflater;
    private ArrayList<BeanStop> stops;
    private HashMap<String, ArrayList<Integer>> lineIdMap;
    private int stopPosition = 0;

    private double rates[] = {0.25, 0.5, 0.75};
    private String[] strRates = {"空车", "舒适", "满座", "拥挤"};

    private int[] dawRates = {
            R.drawable.bus_inside_empty,
            R.drawable.bus_inside_comfortable,
            R.drawable.bus_inside_full,
            R.drawable.bus_inside_crowded
    };

    public InitBusLineRecyclerAdapter(Context context, ArrayList<BeanStop> stops, HashMap<String, ArrayList<Integer>> lineIdMap) {
        this.stops = stops;
        this.context = context;
        this.lineIdMap = lineIdMap;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public InitBusLineRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewHolder itemViewHolder = new ItemViewHolder(mLayoutInflater.inflate(R.layout.item_main_bus_list, parent, false));
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(InitBusLineRecyclerAdapter.ItemViewHolder holder, final int position) {
        final BeanStop stop = stops.get(stopPosition);
        final BeanLine line = stop.getLines().get(position);
        holder.tvBusNo.setText(line.getLine().replace("路", ""));
        holder.tvBusStationName.setText(line.getTerminal());
        holder.tvBusServiceTime.setText(line.getStartTime() + "-" + line.getEndTime());
        ArrayList<BeanBus> buses = line.getBuses();
        if (buses != null && buses.size() > 0) {
            BeanBus bus = BusHelper.getNearBus(buses, stop);
            if (bus.getEta() / 60 > 2) {
                holder.tvBusArrivalTime.setText(bus.getEta() / 60 + "min");
            } else if (bus.getEta() / 60 <= 2) {
                holder.tvBusArrivalTime.setText(ResourcesHelper.getString(context, R.string.text_is_arrviing));
            } else {
                holder.tvBusArrivalTime.setText("--");
            }
            double rate = bus.getRate();
            if (rate < rates[0]) {
                holder.tvBusInsideStatus.setText(strRates[0]);
                holder.imgBusInsideStatus.setImageResource(dawRates[0]);
            } else if (rate > rates[0] && rate < rates[1]) {
                holder.tvBusInsideStatus.setText(strRates[1]);
                holder.imgBusInsideStatus.setImageResource(dawRates[1]);
            } else if (rate > rates[1] && rate < rates[2]) {
                holder.tvBusInsideStatus.setText(strRates[2]);
                holder.imgBusInsideStatus.setImageResource(dawRates[2]);
            } else if (rate > rates[2]) {
                holder.tvBusInsideStatus.setText(strRates[3]);
                holder.imgBusInsideStatus.setImageResource(dawRates[3]);
            } else {
                holder.tvBusInsideStatus.setText("--");
                holder.imgBusInsideStatus.setImageResource(dawRates[0]);
            }
        } else {
            holder.tvBusArrivalTime.setText("--");
            holder.tvBusInsideStatus.setText("--");
            holder.imgBusInsideStatus.setImageResource(dawRates[0]);
        }

        if (TestData.isOpenDemoData(context)) {
            //==============测试代码======
            DecimalFormat df = new DecimalFormat("#.00");
            if(line.getRate() == 0){
                line.setRate(Double.parseDouble(df.format(Math.random())));
            }
            double rate = line.getRate();
                if (rate < rates[0]) {
                    holder.tvBusInsideStatus.setText(strRates[0]);
                    holder.imgBusInsideStatus.setImageResource(dawRates[0]);
                } else if (rate > rates[0] && rate < rates[1]) {
                    holder.tvBusInsideStatus.setText(strRates[1]);
                    holder.imgBusInsideStatus.setImageResource(dawRates[1]);
                } else if (rate > rates[1] && rate < rates[2]) {
                    holder.tvBusInsideStatus.setText(strRates[2]);
                    holder.imgBusInsideStatus.setImageResource(dawRates[2]);
                } else if (rate > rates[2]) {
                    holder.tvBusInsideStatus.setText(strRates[3]);
                    holder.imgBusInsideStatus.setImageResource(dawRates[3]);
                } else {
                    holder.tvBusInsideStatus.setText("--");
                    holder.imgBusInsideStatus.setImageResource(dawRates[0]);
                }
                if(line.getAt() == 0){
                    line.setAt(new Random().nextInt(30));
                }

                int at = line.getAt();
                if (at < 6) {
                    holder.tvBusArrivalTime.setText("即将到达");
                } else {
                    holder.tvBusArrivalTime.setText(at + "min");
                }
                //==============测试代码======

            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LineInfoActivity.class);
                    intent.putExtra(Constants.IntentKey.LINE_ID, lineIdMap.get(line.getLine()));
                    intent.putExtra(Constants.IntentKey.STOP_ID, stop);
                    context.startActivity(intent);
                }
            });
        }

    public void switchStop() {
        if (stopPosition < stops.size() - 1) {
            stopPosition = stopPosition + 1;
        } else if (stopPosition == stops.size() - 1) {
            stopPosition = 0;
        }
    }

    public int getStopPosition() {
        return stopPosition;
    }

    public BeanStop getCurrentStop() {
        return stops.get(stopPosition);
    }

    public void updateStops(ArrayList<BeanStop> stops) {
        this.stops = stops;
        stopPosition = 0;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgBusInsideStatus;
        private TextView tvBusInsideStatus;
        private TextView tvBusNo;
        private TextView tvBusStationName;
        private TextView tvBusServiceTime;
        private TextView tvBusArrivalTime;
        private View view;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            imgBusInsideStatus = (ImageView) itemView.findViewById(R.id.img_bus_inside_status);
            tvBusInsideStatus = (TextView) itemView.findViewById(R.id.tv_bus_inside_status);
            tvBusNo = (TextView) itemView.findViewById(R.id.tv_bus_no);
            tvBusStationName = (TextView) itemView.findViewById(R.id.tv_bus_station_name);
            tvBusServiceTime = (TextView) itemView.findViewById(R.id.tv_bus_service_time);
            tvBusArrivalTime = (TextView) itemView.findViewById(R.id.tv_bus_arrival_time);
        }
    }

    @Override
    public int getItemCount() {
        return stops.get(stopPosition).getLines() != null ? stops.get(stopPosition).getLines().size() : 0;
    }
}
