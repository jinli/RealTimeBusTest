package com.ct.realtimebus.mvp.model.inf;

/**
 * Created by zhangjinli on 16/9/6.
 */
public interface OnDataResultListener {

    void onSuccess(String resultData);
    void onFailure(String msg,Exception e);
}
