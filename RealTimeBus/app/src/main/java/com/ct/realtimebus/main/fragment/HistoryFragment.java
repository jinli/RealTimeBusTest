package com.ct.realtimebus.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.MyApplication;
import com.ct.realtimebus.bean.HistoryBean;
import com.ct.realtimebus.main.adapter.HistoryAdapter;

import java.util.LinkedHashMap;

/**
 * Created by zhangjinli on 16/8/28.
 */
public class HistoryFragment extends Fragment {

    private View mainView;

    private RecyclerView recyclerViewHistory;
    private TextView tvTitle;
    private ImageView imgNoBus;

    private HistoryAdapter historyAdapter;

    private MyApplication myApplication;

    public static HistoryFragment newInstance(){
        HistoryFragment historyFragment = new HistoryFragment();
        return historyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_history,container,false);
        initWidget();
        return mainView;
    }

    public void initWidget(){
        recyclerViewHistory = (RecyclerView)mainView.findViewById(R.id.recycler_history);
        tvTitle = (TextView)mainView.findViewById(R.id.tv_title);
        tvTitle.setText(getActivity().getResources().getString(R.string.text_title_history));
        imgNoBus = (ImageView)mainView.findViewById(R.id.img_no_bus);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApplication = MyApplication.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        LinkedHashMap<Integer,HistoryBean> cache = myApplication.getHistory();
        HistoryBean [] historyBeans = cache.values().toArray(new HistoryBean[]{});
        if(historyBeans == null || historyBeans.length <= 0){
            imgNoBus.setVisibility(View.VISIBLE);
            recyclerViewHistory.setVisibility(View.GONE);
        }else{
            imgNoBus.setVisibility(View.GONE);
            recyclerViewHistory.setVisibility(View.VISIBLE);
        }
        HistoryBean[] newHistoryBeans = new HistoryBean[historyBeans.length];
        for(int i = historyBeans.length - 1; i >=0; i --){
            newHistoryBeans[historyBeans.length -1 - i] = historyBeans[i];
        }
        if(historyAdapter == null){
            historyAdapter = new HistoryAdapter(getActivity(),newHistoryBeans);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewHistory.setLayoutManager(linearLayoutManager);
            recyclerViewHistory.setAdapter(historyAdapter);
        }else{
            historyAdapter.update(newHistoryBeans);
            historyAdapter.notifyDataSetChanged();
        }
    }
}
