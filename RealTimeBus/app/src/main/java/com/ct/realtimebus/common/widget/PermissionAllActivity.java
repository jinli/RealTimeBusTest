package com.ct.realtimebus.common.widget;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ct.realtimebus.common.PermissionHelper;

/**
 * Created by zhangjinli on 16/9/9.
 */
public class PermissionAllActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            PermissionHelper.checkAllPermission(this);
        }
    }
}
