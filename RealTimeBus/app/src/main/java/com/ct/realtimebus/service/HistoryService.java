package com.ct.realtimebus.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.ct.realtimebus.common.MyApplication;
import com.ct.realtimebus.bean.HistoryBean;
import com.ct.realtimebus.common.dbmanager.HistoryDBManager;
import com.ct.realtimebus.splash.activity.SplashActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * create by zhangjinli 2016/08/30
 */
public class HistoryService extends IntentService {
    private static final String TAG = "HistoryService";
    private static final String ACTION_QUERY_HISTORY = "com.ct.realtimebus.main.service.action.QUERY_HISTORY";
    private static final String ACTION_UPDATE_HISTORY = "com.ct.realtimebus.main.service.action.SAVE_HISTORY";

    public HistoryService() {
        super("HistoryService");
    }


    public static void startQueryHistory(Context context) {
        Intent intent = new Intent(context, HistoryService.class);
        intent.setAction(ACTION_QUERY_HISTORY);
        context.startService(intent);
    }

    public static void startUpdateHistory(Context context){
        Intent intent = new Intent(context, HistoryService.class);
        intent.setAction(ACTION_UPDATE_HISTORY);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            HistoryDBManager manager = new HistoryDBManager(getApplicationContext());
            final String action = intent.getAction();
            if (ACTION_QUERY_HISTORY.equals(action)) {
                ArrayList<HistoryBean> historyBeans = manager.queryAll();
                handleActionQuery(historyBeans);
            }else if(ACTION_UPDATE_HISTORY.equals(action)){
                MyApplication myApplication = MyApplication.getInstance();
                LinkedHashMap<Integer,HistoryBean> cache = myApplication.getLruHistory().getCache();
                ArrayList<HistoryBean> historyBeans = new ArrayList<>();
                for(HistoryBean historyBean : cache.values()){
                    historyBeans.add(historyBean);
                }
                manager.deleteAll();
                manager.add(historyBeans);
            }
        }
    }

    private void handleActionQuery(ArrayList<HistoryBean> historyBeans) {
        MyApplication myApplication = MyApplication.getInstance();
        myApplication.getLruHistory().initNodes(historyBeans);
        Intent intent = new Intent(SplashActivity.QUERY_RESUTL);
        sendBroadcast(intent);
    }
}
