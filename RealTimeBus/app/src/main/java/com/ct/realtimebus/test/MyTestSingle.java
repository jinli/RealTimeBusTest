package com.ct.realtimebus.test;

import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;

import org.json.JSONObject;

/**
 * Created by zhangjinli on 16/9/13.
 */
public class MyTestSingle {
    private BeanLine line;
    private BeanStop currentStop;

    public BeanStop getTerminal() {
        return terminal;
    }

    public void setTerminal(BeanStop terminal) {
        this.terminal = terminal;
    }

    private BeanStop terminal;
    private BeanBus nearBus;
    private BeanStop nextStop;
    private double totleDistance;
    private int busCurrentPosition;
    private int currentStopId;

    public static final long period = 800;
    private MyTestSingle(){}

    public static MyTestSingle getInstance(){
        return MyTestSingleHolder.myApplication;
    }

    public BeanStop getCurrentStop() {
        return currentStop;
    }

    public void setCurrentStop(BeanStop currentStop) {
        this.currentStop = currentStop;
    }

    public BeanBus getNearBus() {
        return nearBus;
    }

    public void setNearBus(BeanBus nearBus) {
        this.nearBus = nearBus;
    }

    public BeanLine getLine() {
        return line;
    }

    public void setLine(BeanLine line) {
        this.line = line;
    }

    public int getBusCurrentPosition() {
        return busCurrentPosition;
    }

    public void setBusCurrentPosition(int busCurrentPosition) {
        this.busCurrentPosition = busCurrentPosition;
    }



    public int getCurrentStopId() {
        return currentStopId;
    }

    public void setCurrentStopId(int currentStopId) {
        this.currentStopId = currentStopId;
    }


    public double getTotleDistance() {
        return totleDistance;
    }

    public void setTotleDistance(double totleDistance) {
        this.totleDistance = totleDistance;
    }

    public static class MyTestSingleHolder{
        public static final MyTestSingle myApplication = new MyTestSingle();
    }

    public BeanStop getNextStop() {
        return nextStop;
    }

    public void setNextStop(BeanStop nextStop) {
        this.nextStop = nextStop;
    }
}
