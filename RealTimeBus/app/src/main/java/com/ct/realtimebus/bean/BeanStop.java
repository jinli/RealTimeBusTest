package com.ct.realtimebus.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 公交信息
 * Created by zhangjinli on 16/7/29.
 */
public class BeanStop implements Serializable{
    private ArrayList<BeanBus> buses;
    private String city;
    private String code;
    private int id;
    private double latitude;
    private double longitude;
    private String name;
    private ArrayList<Integer> refs;
    private int sequence;
    private String updated;
    private ArrayList<BeanLine> lines;
    private ArrayList<BeanStop> stops;
    private String toStopName;
    private int userDistance;

    public ArrayList<BeanBus> getBuses() {
        return buses;
    }

    public void setBuses(ArrayList<BeanBus> buses) {
        this.buses = buses;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Integer> getRefs() {
        return refs;
    }

    public void setRefs(ArrayList<Integer> refs) {
        this.refs = refs;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public ArrayList<BeanLine> getLines() {
        return lines;
    }

    public void setLines(ArrayList<BeanLine> lines) {
        this.lines = lines;
    }

    public ArrayList<BeanStop> getStops() {
        return stops;
    }

    public void setStops(ArrayList<BeanStop> stops) {
        this.stops = stops;
    }

    public String getToStopName() {
        return toStopName;
    }

    public void setToStopName(String toStopName) {
        this.toStopName = toStopName;
    }

    public int getUserDistance() {
        return userDistance;
    }

    public void setUserDistance(int userDistance) {
        this.userDistance = userDistance;
    }
}
