package com.ct.realtimebus.reminder.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BaseActivity;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.MyApplication;
import com.ct.realtimebus.main.LruHistory;
import com.ct.realtimebus.reminder.fragment.RemindChooseStopDialogFragment;
import com.ct.realtimebus.mvp.presenter.ReminderPresenter;
import com.ct.realtimebus.service.BusStatuIntentService;
import com.ct.realtimebus.mvp.view.ReminderView;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/10.
 */
public class ReminderSettingActivity extends BaseActivity implements View.OnClickListener, ReminderView, RemindChooseStopDialogFragment.OnChooseStopListener {

    public static final String TAG = "ReminderSettingActivity";
    private static final String PARAM_STOP = "stop";
    private static final String PARAM_STOPS = "stops";
    private static final String PARAM_PATH = "path";
    private static final String PARAM_SIZE = "size";

    private RemindChooseStopDialogFragment fragment;

    private ReminderPresenter reminderPresenter;

    private TextView tvCurrentLine;
    private TextView tvDestinationStopName;
    private TextView tvCurrentStopName;
    private TextView tvSurplusStop;
    private TextView tvEstimatedTime;
    private SwitchCompat switchGetOffRemind;
    private SwitchCompat switchComeBusRemind;
    private Button btnClose;
    private LinearLayout layoutChooseDestinationStop;
    private LinearLayout layoutComeBusSetting;

    private SharedPreferences sharedPreferences;

    private BeanStop currentStop;
    private BeanLine currentLine;

    private BeanBus currentBus;

    private ArrayList<BeanStop> resultStops;
    private ArrayList<BeanBus> resultBuses;
    private BeanLine resultLine;

    private Intent intent;
    private SharedPreferences.Editor editor;

    private BeanStop terminal;
    private int terminalPosition;

    private boolean oldComeBusRemind = false;
    private boolean oldGetOffBus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_setting);
        sharedPreferences = getSharedPreferences(Constants.SHARED_REMIND, 0);//创建用户提醒缓存
        editor = sharedPreferences.edit();
        initWidget();//初始化组件
        intent = getIntent();
        Bundle bundle = intent.getExtras();
        /**储存曾经的变化**/
        oldGetOffBus = sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
        oldComeBusRemind = sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);

        if (!sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false) &&
                !sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {
            currentLine = (BeanLine) bundle.getSerializable(Constants.IntentKey.REMIND_LINE);
            currentStop = (BeanStop) bundle.getSerializable(Constants.IntentKey.REMIND_STOP);
//            remindBus = new JSONObject(bundle.getString(Constants.IntentKey.REMIND_BUS));
        }
        if (sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)) {
            Gson gson = new Gson();
            currentStop = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.CURRENT_STOP, ""), BeanStop.class);
            currentLine = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.REMIND_LINE, ""), BeanLine.class);
            terminal = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.TERMINAL, ""), BeanStop.class);
        }

        if (sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)) {
            Gson gson = new Gson();
            currentStop = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.CURRENT_STOP, ""), BeanStop.class);
            currentLine = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.REMIND_LINE, ""), BeanLine.class);
        }

        reminderPresenter = new ReminderPresenter(this, this);
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_STOPS, "1");
        params.put(PARAM_PATH, "1");
        params.put(PARAM_SIZE, "5");
        int stopId = currentStop.getId();
        int lineId = currentLine.getId();
        params.put(PARAM_STOP, stopId + "");
        reminderPresenter.realTimeRequestBusLine(lineId, params);
    }

    public void initWidget() {
        tvCurrentLine = (TextView) findViewById(R.id.tv_current_line);
        tvDestinationStopName = (TextView) findViewById(R.id.tv_destination_stop_name);
        tvCurrentStopName = (TextView) findViewById(R.id.tv_current_stop_name);
        tvSurplusStop = (TextView) findViewById(R.id.tv_surplus_stops);
        tvEstimatedTime = (TextView) findViewById(R.id.tv_estimated_time);
        switchGetOffRemind = (SwitchCompat) findViewById(R.id.switch_getOffRemind);
        switchComeBusRemind = (SwitchCompat) findViewById(R.id.switch_comeBusRemind);
        btnClose = (Button) findViewById(R.id.btn_close);
        layoutChooseDestinationStop = (LinearLayout) findViewById(R.id.layout_choose_destination_stop);
        layoutComeBusSetting = (LinearLayout) findViewById(R.id.layout_come_bus_setting);
        //初始化OnClick事件
        btnClose.setOnClickListener(this);
        layoutChooseDestinationStop.setOnClickListener(this);
        switchGetOffRemind.setChecked(sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false));
        switchComeBusRemind.setChecked(sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false));

        switchGetOffRemind.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    showText(false);
                } else {
                    showText(true);
                }
            }
        });
    }

    private void showText(boolean show) {
        if (show) {
            tvDestinationStopName.setText(terminal.getName());
            tvSurplusStop.setText(terminalPosition + 1 + "站");
        } else {
            tvDestinationStopName.setText("--");
            tvSurplusStop.setText("--");
            tvEstimatedTime.setText("--");
        }
        layoutChooseDestinationStop.setClickable(show);
    }

    @Override
    public void initReminderSetting(JSONObject result) {
        Gson gson = new Gson();
        resultLine = gson.fromJson(result.toString(), BeanLine.class);
        resultStops = resultLine.getStops();
        resultBuses = resultLine.getBuses();
        resultLine.setAllStops((ArrayList<BeanStop>) resultLine.getStops().clone());
        tvCurrentLine.setText(resultLine.getLine());
        for (int i = 0; i < resultStops.size(); i++) {
            BeanStop stop = resultStops.get(i);
            if (currentStop.getId() == stop.getId()) {
                currentStop.setName(stop.getName());
                tvCurrentStopName.setText(stop.getName());
                resultStops.remove(0);
                break;
            } else {
                resultStops.remove(0);
                i = i - 1;
            }
        }
        if (!sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)) {
            chooseStop(resultStops.get(0), 0);
            showText(false);
        } else {
            for (int i = 0; i < resultStops.size(); i++) {
                if (terminal.getId() == resultStops.get(i).getId()) {
                    chooseStop(terminal, i);
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_close:
                editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, switchComeBusRemind.isChecked());
                editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, switchGetOffRemind.isChecked());
                Gson gson = new Gson();
                if (switchComeBusRemind.isChecked()) {
                    editor.putString(Constants.SharedRemindKey.REMIND_LINE, gson.toJson(resultLine));
                    editor.putString(Constants.SharedRemindKey.CURRENT_STOP, gson.toJson(currentStop));
                    BusStatuIntentService.comeBusRemind(this);
                    //保存历史纪录
                    currentStop.setToStopName(resultLine.getTerminal());
                    saveHistory();
                }

                if (switchGetOffRemind.isChecked()) {
                    editor.putString(Constants.SharedRemindKey.TERMINAL, gson.toJson(terminal));
                    editor.putString(Constants.SharedRemindKey.REMIND_LINE, gson.toJson(resultLine));
                    editor.putString(Constants.SharedRemindKey.CURRENT_STOP, gson.toJson(currentStop));
                    BusStatuIntentService.getOffRemind(this);
                    //保存历史纪录
                    currentStop.setToStopName(terminal.getName());
                    saveHistory();
                }

                editor.commit();

                /**通知设置状态提示**/
                if ((sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)&&!oldComeBusRemind) ||
                        (sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false) && !oldGetOffBus)) {
                    Toast.makeText(this, getResources().getString(R.string.message_success_remind_setting), Toast.LENGTH_SHORT).show();
                } else if ((!sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false) &&oldComeBusRemind) ||
                        (!sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)&&oldGetOffBus)) {
                    Toast.makeText(this, getResources().getString(R.string.nessage_success_remind_cancle), Toast.LENGTH_SHORT).show();
                }
                finish();
                break;
            case R.id.layout_choose_destination_stop:
                fragment = RemindChooseStopDialogFragment.newInstance(resultStops);
                fragment.setOnChooseStopListener(this);
                fragment.show(getSupportFragmentManager(), "dialog");
                break;
        }
    }

    @Override
    public void chooseStop(BeanStop stop, int position) {
        this.terminal = stop;
        this.terminalPosition = position;
        tvDestinationStopName.setText(terminal.getName());
        tvSurplusStop.setText(position + 1 + "站");
        if (fragment != null && fragment.getDialog() != null && fragment.getDialog().isShowing()) {
            fragment.dismiss();
        }
    }

    public void saveHistory() {
        MyApplication.getInstance().addHistory(currentStop, resultLine, LruHistory.SUBSCRIBE);
    }
}
