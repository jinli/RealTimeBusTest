package com.ct.realtimebus.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.main.fragment.MainFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/24.
 */
public class MoveMapStopListAdapter extends RecyclerView.Adapter<MoveMapStopListAdapter.ItemViewHolder> {
    private static final String TAG = "MoveMapStopListAdapter";

    private Context context;
    private ArrayList<BeanStop> stops;
    private LayoutInflater layoutInflater;
    private BeanStop chooseStop;
    private OnItemClickListener onItemClickListener;

    public MoveMapStopListAdapter(Context context, ArrayList<BeanStop> stops) {
        this.context = context;
        this.stops = stops;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_move_map_stop_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        final BeanStop stop = stops.get(position);
        holder.tvStopName.setText(position + 1 +"."+stop.getName());
        holder.tvDistance.setText(stop.getUserDistance() + "米");
        if (chooseStop != null && chooseStop.getId() == stop.getId()) {
            holder.view.setBackgroundColor(ResourcesHelper.getColor(context, R.color.colorBusStationListItemLine));
        } else {
            holder.view.setBackgroundColor(ResourcesHelper.getColor(context,R.color.colorWhite));
        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.itemClick(position);
            }
        });
        holder.tvPokingStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                ArrayList<BeanStop> result = new ArrayList<>();
                result.add(stop);
                resultIntent.putExtra("searchStop", result);
                ((Activity) context).setResult(MainFragment.SEARCH_STOP_RESULTCODE, resultIntent);
                ((Activity) context).finish();
            }
        });
    }

    public void updateData(ArrayList<BeanStop> newData) {
        this.stops = newData;
    }

    public void setChooseStop(BeanStop chooseStop) {
        this.chooseStop = chooseStop;
    }

    @Override
    public int getItemCount() {
        return stops != null ? stops.size() : 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView tvStopName;
        public TextView tvPokingStop;
        public TextView tvDistance;

        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvStopName = (TextView) itemView.findViewById(R.id.tv_stop_name);
            tvPokingStop = (TextView) itemView.findViewById(R.id.tv_poking_stop);
            tvDistance = (TextView) itemView.findViewById(R.id.tv_distance);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void itemClick(int position);
    }
}
