package com.ct.realtimebus.mvp.model;

import android.content.Context;

import com.ct.realtimebus.bean.HistoryBean;
import com.ct.realtimebus.common.dbmanager.HistoryDBManager;
import com.ct.realtimebus.mvp.model.inf.IHistoryModel;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/29.
 */
public class HistoryModel implements IHistoryModel {

    private Context mContext;

    private HistoryDBManager manager;

    public HistoryModel(Context context) {
        this.mContext = context;
        this.manager = new HistoryDBManager(context);
    }

    @Override
    public ArrayList<HistoryBean> queryHistoryByDateDesc() {
        return manager.queryAll();
    }
}
