package com.ct.realtimebus.mvp.model.inf;

import android.content.Context;

import com.android.volley.Response;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public interface ILineModel {

    /**
     * 根据车站id检索线路
     * @param stationId
     * @param params
     */
    void searchLinesByStopId(int stationId,HashMap<String, String> params, ImplDataResultListener onDataResultListener);

    /**
     * 使用线路ID获取线路详情
     * @param lineIds(数组)
     * @param params
     * @param onDataResultListener
     */
    void getLineInfoByIds(ArrayList<Integer> lineIds,HashMap<String,String> params,ImplDataResultListener onDataResultListener);

    /**
     * 使用线路ID获取线路详情
     * @param lineId（单个ID）
     * @param params
     * @param onDataResultListener
     */
    void getLineInfoById(int lineId,HashMap<String,String> params,ImplDataResultListener onDataResultListener);

    void getRealTimeBus(int lineId,HashMap<String,String> params,ImplDataResultListener onDataResultListener);

    void setContext(Context context);

}
