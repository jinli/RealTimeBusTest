package com.ct.realtimebus.common;

/**
 * Created by zhangjinli on 16/8/1.
 */
public interface RecyclerItemClickListener {
    void onRecyclerItemClick(int position);
}
