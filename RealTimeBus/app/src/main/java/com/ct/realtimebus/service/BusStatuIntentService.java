package com.ct.realtimebus.service;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.GetoffAndComeBusInfoDialog;
import com.ct.realtimebus.common.NotificationUtils;
import com.ct.realtimebus.common.RTBUtils;
import com.ct.realtimebus.mvp.model.BusModel;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.model.LineModel;
import com.ct.realtimebus.mvp.model.inf.IBusModel;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;
import com.ct.realtimebus.test.MyTestSingle;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class BusStatuIntentService extends IntentService {
    private static final String TAG = "BusStatuIntentService";
    private static final String ACTION_COMEBUS = "com.ct.realtimebus.reminder.action.COMEBUS";
    private static final String ACTION_GETOFF = "com.ct.realtimebus.reminder.action.GETOFF";
    private static final String PARAM_STOP = "stop";
    private static final String PARAM_STOPS = "stops";
    private static final String PARAM_PATH = "path";
    private static final String PARAM_SIZE = "size";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static ILineModel lineModel = new LineModel();
    private static IBusModel busModel = new BusModel();
    boolean isComeBus = true;
    boolean isGetOff = true;
    private double remindDistance = 500;//提醒距离

    public BusStatuIntentService() {
        super("BusStatuIntentService");
    }

    public static void comeBusRemind(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_REMIND, 0);
            editor = sharedPreferences.edit();
        }
        lineModel.setContext(context);
        busModel.setContext(context);
        Intent intent = new Intent(context, BusStatuIntentService.class);
        intent.setAction(ACTION_COMEBUS);
        context.startService(intent);
    }

    public static void getOffRemind(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_REMIND, 0);
            editor = sharedPreferences.edit();
        }
        lineModel.setContext(context);
        busModel.setContext(context);
        Intent intent = new Intent(context, BusStatuIntentService.class);
        intent.setAction(ACTION_GETOFF);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_COMEBUS.equals(action)) {
                handleActionComeBusRemind();
            } else if (ACTION_GETOFF.equals(action)) {
                handleActionGetOffRemind();
            }
        }
    }

    private void handleActionComeBusRemind() {
        final Gson gson = new Gson();
        final BeanLine line = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.REMIND_LINE, ""), BeanLine.class);
        final BeanStop stop = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.CURRENT_STOP, ""), BeanStop.class);
        int busId = line.getBuses().get(0).getId();
        while (isComeBus) {
            busModel.getBusInfoById(busId, new ImplDataResultListener(getApplicationContext()) {
                @Override
                public void onSuccess(String resultData) {
                    BeanBus bus = gson.fromJson(resultData, BeanBus.class);
                    LatLng busll = new LatLng(bus.getLatitude(),bus.getLongitude());
                    LatLng stopll = new LatLng(stop.getLatitude(), stop.getLongitude());
                    Log.v(TAG,"===================="+AMapUtils.calculateLineDistance(busll,stopll));
                    if (AMapUtils.calculateLineDistance(busll,stopll) <= remindDistance) {
                        isComeBus = false;
                        editor.putBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false);
                        editor.commit();
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("msg", line.getLine() + "即将到达，请准备上车");
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                }
                @Override
                public void onFailure(String msg, Exception e) {
                    super.onFailure(msg, e);
                }
            });
            try {
                Thread.sleep(MyTestSingle.period);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleActionGetOffRemind() {
        final Gson gson = new Gson();
        final BeanLine line = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.REMIND_LINE, ""), BeanLine.class);
        final BeanStop stop = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.CURRENT_STOP, ""), BeanStop.class);
        final BeanStop terminal = gson.fromJson(sharedPreferences.getString(Constants.SharedRemindKey.TERMINAL, ""), BeanStop.class);
        int busId = line.getBuses().get(0).getId();
        while (isGetOff) {
            busModel.getBusInfoById(busId, new ImplDataResultListener(getApplicationContext()) {
                @Override
                public void onSuccess(String resultData) {
                    BeanBus bus = gson.fromJson(resultData, BeanBus.class);
                    if (bus.getSequence() == 1) {
                        isGetOff = false;
                        editor.putBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false);
                        editor.commit();
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("msg", line.getLine() + "即将到达" + terminal.getName() + "，请准备下车");
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                }

                @Override
                public void onFailure(String msg, Exception e) {
                    super.onFailure(msg, e);
                }
            });
            try {
                Thread.sleep(MyTestSingle.period);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    Handler handler = new Handler() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.show(getApplicationContext().getResources().getString(R.string.notification_title), bundle.getString("msg"));
            Intent dialogIntent = new Intent(getApplicationContext(), GetoffAndComeBusInfoDialog.class);
            dialogIntent.putExtras(bundle);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
            super.handleMessage(msg);
        }
    };
}
