package com.ct.realtimebus.common;

import android.content.Context;
import android.os.Build;

import com.ct.realtimebus.R;

/**
 * Created by zhangjinli on 16/9/7.
 */
public class ResourcesHelper {

    public static String getString(Context context,int string){
        return context.getResources().getString(string);
    }

    public static int getColor(Context context,int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getResources().getColor(color,null);
        }else{
            return context.getResources().getColor(color);
        }
    }
}
