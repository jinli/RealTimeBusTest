package com.ct.realtimebus.lineinfo.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.view.View;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.bean.BeanLocation;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BaseActivity;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.test.MyTestSingle;
import com.ct.realtimebus.test.TestButtonActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class LineMapActivity extends BaseActivity implements AMap.OnMarkerClickListener, AMap.OnInfoWindowClickListener, AMap.InfoWindowAdapter {
    private static final String TAG = "LineMapActivity";

    private static final String PATH_LIST = "pathList";
    private static final String STOP_LIST = "stopList";
    private static final String BUS_LIST = "busList";

    private MapView lineMap;
    private AMap aMap;

    private Intent intent;
    private JSONObject stop;
    private BeanLine lineDetail;
    private ArrayList<BeanLocation> paths;
    private ArrayList<BeanStop> stops;
    private ArrayList<BeanBus> buses;
    private BeanStop currentStop;
    private Handler pathHandler;
    private Handler stopHandler;
    private Handler moveHandler;
    private Thread pathThread;
    private Thread stopTread;

    private double rate[] = {0.25, 0.5, 0.75};
    private String[] strRates = {"空车", "舒适", "满座", "拥挤"};

    private Timer timer = new Timer();
    private TimerTask timerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_map);
        lineMap = (MapView) findViewById(R.id.map_line);
        lineMap.onCreate(savedInstanceState);
        aMap = lineMap.getMap();
        aMap.setOnInfoWindowClickListener(this);
        aMap.setOnMarkerClickListener(this);
        aMap.setInfoWindowAdapter(this);
        initHanderl();
        initTimerTask();
        intent = getIntent();

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            lineDetail = (BeanLine) bundle.getSerializable(Constants.IntentKey.LINE_DETAIL);
            currentStop = (BeanStop) bundle.getSerializable(Constants.IntentKey.STOP_DETAIL);
            paths = lineDetail.getPaths();
            stops = lineDetail.getStops();
            buses = lineDetail.getBuses();
            pathThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ArrayList<LatLng> list = new ArrayList<>();
                    for (BeanLocation beanLocation : paths) {
                        LatLng latLng = new LatLng(beanLocation.getLat(), beanLocation.getLng());
                        list.add(latLng);
                    }
                    Message message = new Message();
                    Bundle pathBundle = new Bundle();
                    pathBundle.putParcelableArrayList(PATH_LIST, list);
                    message.setData(pathBundle);
                    pathHandler.sendMessage(message);
                }
            });
            pathThread.start();

            stopTread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ArrayList<MarkerOptions> stopOptionses = new ArrayList<>();
                    ArrayList<MarkerOptions> busOptionses = new ArrayList<>();
                    //生成车站Marker集合
                    for (int i = 0; i < stops.size(); i++) {
                        BeanStop markerStop = stops.get(i);
                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(markerStop.getLatitude(), markerStop.getLongitude());
                        BitmapDescriptor bitmapDescriptor = null;
                        //获取标记资源图片
                        BitmapDescriptorFactory bitmapDescriptorFactory = new BitmapDescriptorFactory();
                        if (i == 0) {
                            bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.map_start_point);
                        } else if (i == stops.size() - 1) {
                            bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.map_end_point);
                        } else {
                            bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.map_stop_point);
                        }
                        markerOptions.position(latLng);
                        markerOptions.icon(bitmapDescriptor);
                        markerOptions.snippet(markerStop.getName()).draggable(true);
                        stopOptionses.add(markerOptions);

                    }
                    //生成车辆Marker集合
                    if (buses != null) {
                        for (int i = 0; i < buses.size(); i++) {
                            BeanBus bus = buses.get(i);
                            /**犹豫数据不准确，对bus进行单独的判断*/
                            if (bus.getLineId() == lineDetail.getId()) {
                                MarkerOptions markerOptions = new MarkerOptions();
                                LatLng latLng = new LatLng(bus.getLatitude(), bus.getLongitude());
                                /** Test Code to simulation the bus location*/
//                                    latLng = stopOptionses.get(stopOptionses.size() / 3 + i).getPosition();
                                /** Test Code to simulation the bus location*/
                                //获取车辆资源图片
                                BitmapDescriptor bitmapDescriptor = null;
                                BitmapDescriptorFactory bitmapDescriptorFactory = new BitmapDescriptorFactory();
                                double busRate = bus.getRate();
                                StringBuffer sb = new StringBuffer();
                                if (busRate < rate[0]) {
                                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_empty);
                                    sb.append(strRates[0]);
                                } else if (busRate > rate[0] && busRate < rate[1]) {
                                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_comfortable);
                                    sb.append(strRates[0]);
                                } else if (busRate > rate[1] && busRate < rate[2]) {
                                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_full);
                                    sb.append(strRates[0]);
                                } else if (busRate > rate[2]) {
                                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_crowded);
                                    sb.append(strRates[0]);
                                }
                                sb.append("\n");
                                sb.append("预计到达时间：--");
                                markerOptions.position(latLng);
                                markerOptions.snippet(sb.toString()).draggable(true);
                                markerOptions.icon(bitmapDescriptor);
                                busOptionses.add(markerOptions);
                            }
                        }
                    }
                    Message message = new Message();
                    Bundle stopBundle = new Bundle();
                    stopBundle.putParcelableArrayList(STOP_LIST, stopOptionses);
                    stopBundle.putParcelableArrayList(BUS_LIST, busOptionses);
                    message.setData(stopBundle);
                    stopHandler.sendMessage(message);
                }
            });
            stopTread.start();
        } else {

        }
    }

    Marker marker = null;

    public void initHanderl() {
        pathHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle pathBundle = msg.getData();
                ArrayList<LatLng> list = pathBundle.getParcelableArrayList(PATH_LIST);
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.color(getResources().getColor(R.color.colorMapPolyLine));
                polylineOptions.addAll(list);
                //绘制路径
                aMap.addPolyline(polylineOptions);
                super.handleMessage(msg);
            }
        };

        stopHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle stopBundler = msg.getData();
                ArrayList<MarkerOptions> stopList = stopBundler.getParcelableArrayList(STOP_LIST);
                ArrayList<MarkerOptions> busList = stopBundler.getParcelableArrayList(BUS_LIST);
                aMap.addMarkers(stopList, true);//返回多个Markers对象 备用
                aMap.addMarkers(busList, false);
                /**犹豫数据不准确，对bus进行单独的判断*/
                if (busList != null && busList.size() > 0) {
                    aMap.addMarkers(busList, false);
                }
                //添加我的位置
                MarkerOptions markerOptions = new MarkerOptions();
                //获取标记资源图片
                BitmapDescriptorFactory bitmapDescriptorFactory = new BitmapDescriptorFactory();
                markerOptions.icon(bitmapDescriptorFactory.fromResource(R.drawable.route_map_my_location));
                LatLng latLng = new LatLng(currentStop.getLatitude(), currentStop.getLongitude());
                markerOptions.position(latLng);
                aMap.addMarker(markerOptions);
                super.handleMessage(msg);
            }
        };
        moveHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                MyTestSingle mts = MyTestSingle.getInstance();
                BeanBus bus = mts.getNearBus();
                MarkerOptions options = new MarkerOptions();
                LatLng latLng = new LatLng(bus.getLatitude(), bus.getLongitude());
                options.position(latLng);
                //获取车辆资源图片
                BitmapDescriptor bitmapDescriptor = null;
                BitmapDescriptorFactory bitmapDescriptorFactory = new BitmapDescriptorFactory();
                double busRate = bus.getRate();
                if (busRate < rate[0]) {
                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_empty);
                } else if (busRate > rate[0] && busRate < rate[1]) {
                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_comfortable);
                } else if (busRate > rate[1] && busRate < rate[2]) {
                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_full);
                } else if (busRate > rate[2]) {
                    bitmapDescriptor = bitmapDescriptorFactory.fromResource(R.drawable.route_map_crowded);
                }
                options.icon(bitmapDescriptor);
                if (marker != null) {
                    marker.remove();
                }
                marker = aMap.addMarker(options);
            }
        };
    }

    private void initTimerTask() {
        SharedPreferences remindSp = this.getSharedPreferences(Constants.SHARED_REMIND, 0);
        SharedPreferences testSp = this.getSharedPreferences(TestButtonActivity.TEST_SETTING, 0);
        timerTask = new MyTimerTask();
        if (testSp.getBoolean(TestButtonActivity.DEMO_DATA, false) && (remindSp.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false) || remindSp.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false))) {
            timer.schedule(timerTask, 1000, MyTestSingle.period);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        getInfoWindow(marker);
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        lineMap.onPause();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        lineMap.onDestroy();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        lineMap.onResume();
        if (timer == null) {
            timer = new Timer();
            initTimerTask();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (timer == null) {
            timer = new Timer();
            initTimerTask();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        lineMap.onSaveInstanceState(outState);
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            moveHandler.sendEmptyMessage(0);
        }
    }
}
