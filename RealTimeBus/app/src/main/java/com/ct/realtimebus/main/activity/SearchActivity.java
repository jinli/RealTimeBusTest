package com.ct.realtimebus.main.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.text.Editable;
import android.text.TextPaint;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.BaseActivity;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.DialogUtils;
import com.ct.realtimebus.common.PermissionHelper;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.widget.CustomSlideView;
import com.ct.realtimebus.main.adapter.MoveMapStopListAdapter;
import com.ct.realtimebus.main.custom.CustomRecyclerView;
import com.ct.realtimebus.main.custom.CustomSearchStopsPopupWindow;
import com.ct.realtimebus.main.fragment.MainFragment;
import com.ct.realtimebus.mvp.presenter.SearchPresenter;
import com.ct.realtimebus.mvp.view.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchActivity extends BaseActivity implements SearchView,
        LocationSource, AMapLocationListener, AMap.OnMarkerClickListener,
        AMap.OnMapClickListener {

    public static final String TAG = "SearchActivity";

    private HashMap<String, String> params = new HashMap<>();
    private ArrayList<Marker> stopMarkers;
    private LatLngBounds currentBounds;

    private MapView mapView;
    private LinearLayout searchBar;
    private EditText viewTextEdit;
    private CustomSearchStopsPopupWindow searchStopsPopupWindow;
    private ProgressDialog loadingDialog;
    private CustomSlideView slideView;
    private View stoplistView;
    private RelativeLayout mapParentView;

    private CustomRecyclerView recyclerStops;
    private SearchPresenter searchPresenter;

    private AMap aMap;
    private OnLocationChangedListener onLocationChangedListener;
    private AMapLocationClient aMapLocationClient;
    private AMapLocationClientOption aMapLocationClientOption;

    private double realTimeLat;
    private double realTimeLng;

    private LatLng userClickLatLng = null;
    private ArrayList<BeanStop> stops;
    private MoveMapStopListAdapter moveMapStopListAdapter;

    private Handler updateMapHandler;

    private int clickPosition = 0;

    private VelocityTracker velocityTracker;
    private float recyclerStartY;
    private float recyclerEndY;
    private float distance = 150;
    private float speed = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchPresenter = new SearchPresenter(this, this);
        /*** 初始化组件**/
        initWidget();
        initMap();
        mapView.onCreate(savedInstanceState);
        initHandler();
    }

    /**
     * 初始化组件
     */
    private void initWidget() {
        searchBar = (LinearLayout) findViewById(R.id.search_bar);
        viewTextEdit = (EditText) findViewById(R.id.view_text_edit);
        slideView = (CustomSlideView) findViewById(R.id.slideView);

        searchStopsPopupWindow = new CustomSearchStopsPopupWindow(this);
        searchStopsPopupWindow.setContentView(getLayoutInflater().inflate(R.layout.popupwindow_search_stop_list, null));
        searchStopsPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        searchStopsPopupWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        searchStopsPopupWindow.setFocusable(true);
        searchStopsPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        searchStopsPopupWindow.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
        searchStopsPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        stoplistView = LayoutInflater.from(this).inflate(R.layout.layout_search_activity_slide_view, null);
        slideView.setContentView(stoplistView);
        recyclerStops = (CustomRecyclerView) stoplistView.findViewById(R.id.recycler_stops);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerStops.setLayoutManager(linearLayoutManager);
        mapParentView = (RelativeLayout) findViewById(R.id.map_parent_view);
        moveMapStopListAdapter = new MoveMapStopListAdapter(this, null);
        recyclerStops.setAdapter(moveMapStopListAdapter);
        /**绑定事件**/
        bindWidget();
    }

    private void bindWidget() {
        /**绑定列表点击事件**/
        moveMapStopListAdapter.setOnItemClickListener(new MoveMapStopListAdapter.OnItemClickListener() {
            @Override
            public void itemClick(int position) {
                BeanStop chooseStop = stops.get(position);
                List<Marker> markers = aMap.getMapScreenMarkers();
                clickPosition = position;
                onMarkerClick(stopMarkers.get(position));
            }
        });

        /**绑定点击事件**/
        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStopsPopupWindow.showAtLocation(searchBar, Gravity.TOP, 0, 0);
                inputMethod();
            }
        });

        viewTextEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStopsPopupWindow.showAtLocation(searchBar, Gravity.TOP, 0, 0);
                inputMethod();
            }
        });

        slideView.setSlideStatusListener(new CustomSlideView.SlideStatusListener() {
            @Override
            public void complateSlide(int position) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mapView.getLayoutParams();
                layoutParams.setMargins(0, 0, 0, slideView.getSlideBarHeight());
                LinearLayout.LayoutParams parentParams = (LinearLayout.LayoutParams) mapParentView.getLayoutParams();
                if (position == CustomSlideView.IS_CENTER) {
                    parentParams.height = (slideView.getTotalHeight() - slideView.getSlideBarHeight()) / 2 - slideView.getSlideBarHeight();
                } else if (position == CustomSlideView.IS_BOTTOM) {
                    parentParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
                }
                mapParentView.setLayoutParams(parentParams);
                mapView.setLayoutParams(layoutParams);
                aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(currentBounds, 30));
            }
        });

        recyclerStops.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(velocityTracker == null){
                        velocityTracker = VelocityTracker.obtain();
                    }else{
                        velocityTracker.clear();
                    }
                    velocityTracker.addMovement(event);
                    recyclerStartY = event.getRawY();
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    velocityTracker.addMovement(event);
                    velocityTracker.computeCurrentVelocity(500);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    recyclerEndY = event.getRawY();
                    float currentSpeed = velocityTracker.getYVelocity();
                    float currentDistance = recyclerEndY - recyclerStartY;
                    switch (slideView.getNowStatu()){
                        case CustomSlideView.IS_TOP:
                            if(currentDistance > 0){//向下滑动
                                if(currentSpeed >= speed && currentDistance >= distance){
                                    slideView.changeView(CustomSlideView.IS_CENTER);
                                }
                            }
                            break;
                        case CustomSlideView.IS_CENTER:
                            if(currentDistance > 0){//向下滑动
                                if(currentSpeed >= speed && currentDistance >= distance){
                                    slideView.changeView(CustomSlideView.IS_BOTTOM);
                                }
                            }else if(currentDistance < 0){//向上滑动
                                if(currentSpeed <= speed*-1 && currentDistance <= distance*-1){
                                    slideView.changeView(CustomSlideView.IS_TOP);
                                }
                            }
                            break;
                        case CustomSlideView.IS_BOTTOM:
                            break;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent event) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });
    }

    /**
     * 键盘处理事件
     */
    private void inputMethod() {
        //弹出键盘
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 初始化地图组件
     */
    private void initMap() {
        WindowManager windowManager = getWindowManager();
        //获取屏幕宽高
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        mapView = (MapView) findViewById(R.id.map_view);
        aMap = mapView.getMap();
        aMap.setPointToCenter(width / 2, height / 4);
        aMap.setLocationSource(this);
        aMap.getUiSettings().setMyLocationButtonEnabled(true);//设置默认定位按钮是否显示
        aMap.getUiSettings().setScaleControlsEnabled(true);
        aMap.setMyLocationEnabled(true);//设置显示定位层并可出发定位
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        aMap.setOnMarkerClickListener(this);
        aMap.setOnMapClickListener(this);

        /**
        *根据自定义SlideView状态，初始化地图高度
        */
        aMap.setOnMapLoadedListener(new AMap.OnMapLoadedListener() {
            @Override
            public void onMapLoaded() {
                if(slideView.getNowStatu() == CustomSlideView.IS_CENTER){
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mapView.getLayoutParams();
                    layoutParams.setMargins(0, 0, 0, slideView.getSlideBarHeight());
                    LinearLayout.LayoutParams parentParams = (LinearLayout.LayoutParams) mapParentView.getLayoutParams();
                    if (slideView.getNowStatu() == CustomSlideView.IS_CENTER) {
                        parentParams.height = (slideView.getTotalHeight() - slideView.getSlideBarHeight()) / 2 - slideView.getSlideBarHeight();
                    } else if (slideView.getNowStatu() == CustomSlideView.IS_BOTTOM) {
                        parentParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
                    }
                    mapParentView.setLayoutParams(parentParams);
                    mapView.setLayoutParams(layoutParams);
//                    aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(currentBounds, 30));
                }
            }
        });

    }

    //=========================Custom Method

    @Override
    public void updateSearchStopList(ArrayList<BeanStop> stops) {

    }

    /**
     * 在bitmap上绘制文字
     * @param pm_val
     * @return
     */
    protected Bitmap getMyBitmap(String pm_val) {
        Bitmap bitmap = BitmapDescriptorFactory.fromResource(
                R.drawable.map_stop_point).getBitmap();
        bitmap = Bitmap.createBitmap(bitmap, 0 ,0, bitmap.getWidth(),
                bitmap.getHeight());
        Canvas canvas = new Canvas(bitmap);
        TextPaint textPaint = new TextPaint();
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(22f);
        textPaint.setColor(getResources().getColor(R.color.colorWhite));
        canvas.drawText(pm_val, bitmap.getWidth()/3, bitmap.getHeight()/2, textPaint);// 设置bitmap上面的文字位置
        return bitmap;
    }

    /**
     * 网络请求回调 开启子线程更新Map中的站点信息
     *
     * @param resultStops
     */
    @Override
    public void updateMap(ArrayList<BeanStop> resultStops) {
        stops = resultStops;
        new Thread(new Runnable() {
            @Override
            public void run() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                ArrayList<MarkerOptions> markerOptionsArrayList = new ArrayList<>();
                for (int i = 0; i < stops.size(); i++) {
                    BeanStop stop = stops.get(i);
                    MarkerOptions markerOptions = new MarkerOptions();
                    BitmapDescriptorFactory iconFactory = new BitmapDescriptorFactory();
                    BitmapDescriptor icon = iconFactory.fromBitmap(getMyBitmap(i + 1 +""));
                    LatLng latLng = new LatLng(stop.getLatitude(), stop.getLongitude());
                    builder.include(latLng);
                    markerOptions.position(latLng);
                    markerOptions.icon(icon);
                    markerOptions.snippet(stop.getName()).draggable(true);
                    markerOptionsArrayList.add(markerOptions);

                    //计算用户到各站点的直线距离并存入Stop集合
                    if (userClickLatLng != null) {
                        float distance = AMapUtils.calculateLineDistance(userClickLatLng, latLng);
                        stops.get(i).setUserDistance((int) distance);
                    } else {
                        float distance = AMapUtils.calculateLineDistance(new LatLng(realTimeLat, realTimeLng), latLng);
                        stops.get(i).setUserDistance((int) distance);
                    }
                }
                Message message = new Message();
                Bundle data = new Bundle();
                data.putParcelableArrayList("markers", markerOptionsArrayList);
                data.putParcelable("bounds", builder.build());
                message.setData(data);
                updateMapHandler.sendMessage(message);
            }
        }).start();
    }

    /**
     * 初始化Handler
     */
    private void initHandler() {
        //用于子线程组装站点集合后 更新地图UI
        updateMapHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                boolean isCenter = false;
                Bundle data = msg.getData();
                ArrayList<MarkerOptions> currentMarkers = data.getParcelableArrayList("markers");
                currentBounds = data.getParcelable("bounds");
                if (userClickLatLng != null) {
                    MarkerOptions clickMarker = new MarkerOptions();
                    clickMarker.position(userClickLatLng);
                    currentMarkers.add(clickMarker);
                    aMap.clear(true);
//                    aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));
                } else {
                    isCenter = true;
                }

                stopMarkers = aMap.addMarkers(currentMarkers, false);
                if (userClickLatLng != null) {
                    aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(currentBounds, 30));
//                    aMap.moveCamera(CameraUpdateFactory.changeLatLng(userClickLatLng));
//                    aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userClickLatLng,14));
                } else {
                    aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(currentBounds, 30));
                }
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }

                //更新列表
                updateStopsList(stops);
                super.handleMessage(msg);
            }
        };
    }

    private void updateStopsList(ArrayList<BeanStop> stops) {
        moveMapStopListAdapter.updateData(stops);
        moveMapStopListAdapter.notifyDataSetChanged();
    }

    //====================Map Method

    /**
     * 地图点击事件
     *
     * @param latLng
     */
    @Override
    public void onMapClick(LatLng latLng) {
        userClickLatLng = latLng;
        params.put(Constants.PARAM_NAME_LAT, latLng.latitude + "");
        params.put(Constants.PARAM_NAME_LNG, latLng.longitude + "");
        loadingDialog = DialogUtils.showLoading(this);
        searchPresenter.getStopsByLocation(params);
    }

    /**
     * Marker点击事件
     *
     * @param marker
     * @return
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        LatLng latLng = marker.getPosition();
        for (int i = 0; i < stopMarkers.size(); i++) {
            Marker stopMarker = stopMarkers.get(i);
            if (stopMarker.getPosition().latitude == latLng.latitude && stopMarker.getPosition().longitude == latLng.longitude) {
                clickPosition = i;
                break;
            }
        }
        BeanStop chooseStop = stops.get(clickPosition);
        moveMapStopListAdapter.setChooseStop(chooseStop);
        moveMapStopListAdapter.notifyDataSetChanged();
        recyclerStops.scrollToPosition(clickPosition);
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(latLng));
        return true;
    }

    /**
     * 定位监听
     *
     * @param aMapLocation
     */
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (onLocationChangedListener != null && aMapLocation != null) {
            if (aMapLocation != null
                    && aMapLocation.getErrorCode() == 0) {
                onLocationChangedListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
                realTimeLat = aMapLocation.getLatitude();
                realTimeLng = aMapLocation.getLongitude();
                //检索定位点附近站点
                params.put(Constants.PARAM_NAME_QUERY, "");
                params.put(Constants.PARAM_NAME_LAT, realTimeLat + "");
                params.put(Constants.PARAM_NAME_LNG, realTimeLng + "");
                loadingDialog = DialogUtils.showLoading(this);
                searchPresenter.getStopsByLocation(params);
//                aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(realTimeLat, realTimeLng), 14));
            } else {
                String errText = "定位失败," + aMapLocation.getErrorCode() + ": " + aMapLocation.getErrorInfo();
                Log.e("AmapErr", errText);
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        this.onLocationChangedListener = onLocationChangedListener;
        if (aMapLocationClient == null) {
            aMapLocationClient = new AMapLocationClient(this);
            aMapLocationClientOption = new AMapLocationClientOption();
            aMapLocationClientOption.setOnceLocation(true);//设置只定位一次
            aMapLocationClientOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//设置高精度定位模式
            aMapLocationClient.setLocationListener(this);
            aMapLocationClient.setLocationOption(aMapLocationClientOption);
        }
        /**检查用户权限**/
        if (!PermissionHelper.checkPermission(this, PermissionHelper.PERMISSION_COARSE_LOCATION)) {
            PermissionHelper.requestPermission(this, PermissionHelper.ACCESS_LOCATION_REQUEST_CODE, PermissionHelper.PERMISSION_COARSE_LOCATION);
        } else {
            aMapLocationClient.startLocation();
        }
    }

    @Override
    public void deactivate() {
        onLocationChangedListener = null;
        if (aMapLocationClient != null) {
            aMapLocationClient.stopLocation();
            aMapLocationClient.onDestroy();
        }
        aMapLocationClient = null;
    }

    //==================Activity Method

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionHelper.ACCESS_LOCATION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    aMapLocationClient.startLocation();
                } else {
                    Toast.makeText(this, ResourcesHelper.getString(this, R.string.message_error_no_location), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        deactivate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (aMapLocationClient != null) {
            aMapLocationClient.onDestroy();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mapView.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, slideView.getSlideBarHeight());
        layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        mapView.setLayoutParams(layoutParams);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }
}
