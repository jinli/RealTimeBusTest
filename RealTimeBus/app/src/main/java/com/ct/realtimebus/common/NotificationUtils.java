package com.ct.realtimebus.common;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;

import com.ct.realtimebus.R;

/**
 * Created by zhangjinli on 16/9/6.
 */
public class NotificationUtils {

    private Context mContext;
    private NotificationManagerCompat notificationManager;
    private Notification.Builder builder;

    public NotificationUtils(Context context){
        this.mContext = context;
        this.notificationManager = NotificationManagerCompat.from(mContext);
        this.builder = new Notification.Builder(mContext);
    }

    public void show(String title,String contentText){
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setAutoCancel(true);
        builder.setContentTitle(title);
        builder.setContentText(contentText);
        builder.setWhen(System.currentTimeMillis());
//        builder.setTicker("new message");
        builder.setOngoing(true);
        builder.setNumber(20);
        builder.setDefaults(Notification.DEFAULT_ALL);
        Notification notification = builder.build();
        notification.defaults = Notification.DEFAULT_ALL;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
    }
}
