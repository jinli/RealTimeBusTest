package com.ct.realtimebus.mvp.presenter;

import android.content.Context;

import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.model.LineModel;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;
import com.ct.realtimebus.mvp.view.ReminderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.logging.Handler;

/**
 * Created by zhangjinli on 16/8/16.
 */
public class ReminderPresenter {

    private Context mContext;
    private ILineModel lineModel;
    private ReminderView reminderView;

    public ReminderPresenter(Context context,ReminderView reminderView) {
        this.mContext = context;
        this.reminderView = reminderView;
        this.lineModel = new LineModel();
        this.lineModel.setContext(context);
    }

    public void realTimeRequestBusLine(int lineId,HashMap<String,String> params){
        lineModel.getLineInfoById(lineId, params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String result) {
                try {
                    reminderView.initReminderSetting(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(String msg, Exception e) {

            }
        });
    }
}
