package com.ct.realtimebus.mvp.model.inf;

import android.content.Context;

import com.android.volley.Response;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;

import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public interface IStopModel {
    /**
     * 查询附近的车站
     * @param params
     */
    void getNearbyStops(HashMap<String, String> params,ImplDataResultListener onDataResultListener);

    /**
     * 根据关键字检索车站
     * @param params
     * @param onDataResultListener
     */
    void searchStopsByKeywords(HashMap<String,String> params,ImplDataResultListener onDataResultListener);


    void setContext(Context context);
}
