package com.ct.realtimebus.reminder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by zhangjinli on 16/8/15.
 */
public class RemindChooseStopAdapter extends RecyclerView.Adapter<RemindChooseStopAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<BeanStop> stops;

    private LayoutInflater layoutInflater;

    private ItemClickListener itemClickListener;

    public RemindChooseStopAdapter(Context context, ArrayList<BeanStop> stops) {
        this.mContext = context;
        this.stops = stops;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_remind_stop_list,parent,false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
            holder.tvStop.setText(stops.get(position).getName());
//            if(position%2!=0){
//                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
//            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position);
                }
            });
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return stops != null ? stops.size() : 0;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        public TextView tvStop;
        public View view;
        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvStop = (TextView)itemView.findViewById(R.id.tv_stop);
        }
    }

    public interface ItemClickListener{
        void onItemClick(int position);
    }
}
