package com.ct.realtimebus.main.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.ct.realtimebus.R;
import com.ct.realtimebus.bean.BeanStop;
import com.ct.realtimebus.common.Constants;
import com.ct.realtimebus.common.widget.IconFontButton;
import com.ct.realtimebus.main.adapter.SearchStopRecyclerAdapter;
import com.ct.realtimebus.main.fragment.MainFragment;
import com.ct.realtimebus.mvp.presenter.SearchPresenter;
import com.ct.realtimebus.mvp.view.SearchView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public class CustomSearchStopsPopupWindow extends PopupWindow implements View.OnClickListener,TextWatcher,SearchView{

    private Context mContext;
    private View view;

    private SearchStopRecyclerAdapter searchStopRecyclerAdapter;
    private RecyclerView recyclerStop;
    private IconFontButton btnResetEdit;
    private EditText searchEdit;
    private SearchPresenter searchPresenter;
    private HashMap<String,String> params = new HashMap<>();

    private ArrayList<BeanStop> stops = new ArrayList<>();//搜索框stop集合

    public CustomSearchStopsPopupWindow(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public void setContentView(View contentView) {
        searchPresenter = new SearchPresenter(mContext,this);
        view = contentView;
        recyclerStop = (RecyclerView)view.findViewById(R.id.recycler_stop);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        searchEdit = (EditText)view.findViewById(R.id.popup_window_search_edit);
        btnResetEdit = (IconFontButton)view.findViewById(R.id.btn_resut_edit);
        btnResetEdit.setOnClickListener(this);
        searchEdit.addTextChangedListener(this);
        recyclerStop.setLayoutManager(linearLayoutManager);
        searchStopRecyclerAdapter = new SearchStopRecyclerAdapter(mContext,stops);
        recyclerStop.setAdapter(searchStopRecyclerAdapter);
        searchStopRecyclerAdapter.setmRecyclerItemClickListener(new SearchStopRecyclerAdapter.RecyclerItemClickListener() {
            @Override
            public void onRecyclerItemClick(int position) {
                BeanStop searchStop = stops.get(position);
                for(BeanStop stps : searchStop.getStops()){
                    stps.setName(searchStop.getName());
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra("searchStop", searchStop.getStops());
                ((Activity)mContext).setResult(MainFragment.SEARCH_STOP_RESULTCODE, resultIntent);
                ((Activity)mContext).finish();
            }
        });
        super.setContentView(view);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s == null || s.toString() == null || s.toString().equals("")){
            btnResetEdit.setVisibility(View.INVISIBLE);
            updateSearchStopList(null);
        }else{
            btnResetEdit.setVisibility(View.VISIBLE);
            params.put(Constants.PARAM_NAME_CITY, Constants.TEST_CURRENT_CITY);
            params.put(Constants.PARAM_NAME_LENGTH, Constants.TEST_LENGTH);
            params.put(Constants.PARAM_NAME_QUERY, s.toString());
            searchPresenter.searchStopsByKeywords(params);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_resut_edit:
                searchEdit.setText("");
                break;
        }
    }

    @Override
    public void updateSearchStopList(ArrayList<BeanStop> stops) {
        this.stops = stops;
        searchStopRecyclerAdapter.updateData(stops);
        searchStopRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateMap(ArrayList<BeanStop> resultStops) {

    }
}
