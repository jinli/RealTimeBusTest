package com.ct.realtimebus.test;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ct.realtimebus.R;
import com.ct.realtimebus.common.Constants;

public class TestButtonActivity extends AppCompatActivity {
    public static final String TEST_SETTING = "testSetting";
    public static final String DEMO_DATA = "demoData";

    private Button btnOpenDemoData;
    private Button btnMoveBus;
    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_button);
        sp = getSharedPreferences(TEST_SETTING, 0);
        boolean dd = sp.getBoolean(DEMO_DATA, false);
        btnOpenDemoData = (Button) findViewById(R.id.open_demo_data);
        if (dd) {
            btnOpenDemoData.setText("关闭演示数据");
        } else {
            btnOpenDemoData.setText("打开演示数据");
        }
        btnMoveBus = (Button) findViewById(R.id.btn_move_bus);
        bindListener();
    }

    private void bindListener() {
        btnOpenDemoData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean dd = sp.getBoolean(DEMO_DATA, false);
                SharedPreferences.Editor editor = sp.edit();
                if (dd) {
                    editor.putBoolean(DEMO_DATA, false);
                    btnOpenDemoData.setText("打开演示数据");
                } else {
                    editor.putBoolean(DEMO_DATA, true);
                    btnOpenDemoData.setText("关闭演示数据");
                }
                editor.commit();
            }
        });

        btnMoveBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_REMIND, 0);
                if (sharedPreferences.getBoolean(Constants.SharedRemindKey.COME_BUS_REMIND, false)
                        || sharedPreferences.getBoolean(Constants.SharedRemindKey.GET_OFF_REMIND, false)) {
                    Intent intent = new Intent();
                    intent.setAction(TestService.ACTION_FOO);
                    TestService.move(TestButtonActivity.this);
                    finish();
                }else{
                    Toast.makeText(TestButtonActivity.this,"请设置上车或下车提醒",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
