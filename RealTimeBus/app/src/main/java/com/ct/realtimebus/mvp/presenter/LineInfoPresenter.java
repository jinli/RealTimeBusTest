package com.ct.realtimebus.mvp.presenter;

import android.content.Context;

import com.ct.realtimebus.bean.BeanBus;
import com.ct.realtimebus.bean.BeanLine;
import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.view.LineInfoView;
import com.ct.realtimebus.mvp.model.LineModel;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/8.
 */
public class LineInfoPresenter {

    private Context mContext;

    private ILineModel lineModel;
    private LineInfoView lineInfoView;

    private HashMap<String,String> params = new HashMap<>();//参数集合

    public LineInfoPresenter(Context context,LineInfoView lineInfoView) {
        this.mContext = context;
        this.lineModel = new LineModel();
        this.lineInfoView = lineInfoView;
        lineModel.setContext(mContext);
    }

    public void getLineInfoById(ArrayList<Integer> lineIds,HashMap<String,String> params){

       lineModel.getLineInfoByIds(lineIds, params, new ImplDataResultListener(mContext) {
           @Override
           public void onSuccess(String result) {
               Type type = new TypeToken<ArrayList<BeanLine>>() {
               }.getType();
               Gson gson = new Gson();
               ArrayList<BeanLine> lines = gson.fromJson(result, type);
               lineInfoView.initBusLineTimeLine(lines);
           }

           @Override
           public void onFailure(String msg, Exception e) {
               //TODO 集中处理错误
           }
       });
    }

    public void getRealTimeBus(int lineId ,HashMap<String,String> params){
        lineModel.getRealTimeBus(lineId, params, new ImplDataResultListener(mContext) {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<BeanBus>>(){}.getType();
                ArrayList<BeanBus> buses = gson.fromJson(result,type);
                lineInfoView.updateTimeBusStatus(buses);
            }
        });

    }

}
