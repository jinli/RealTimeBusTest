package com.ct.realtimebus.mvp.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ct.realtimebus.R;
import com.ct.realtimebus.common.ResourcesHelper;
import com.ct.realtimebus.common.TestData;
import com.ct.realtimebus.common.VolleyHelper;
import com.ct.realtimebus.common.widget.RestRequestConstants;
import com.ct.realtimebus.mvp.model.inf.IStopModel;
import com.ct.realtimebus.mvp.model.inf.OnDataResultListener;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by zhangjinli on 16/8/19.
 */
public class StopModel implements IStopModel {

    private Context mContext;

    @Override
    public void getNearbyStops(HashMap<String, String> params, final ImplDataResultListener onDataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            VolleyHelper volleyHelper = new VolleyHelper(mContext);
            volleyHelper.createRequest(RestRequestConstants.MAIN_NEAR_STATION_BY_LOCATION, Request.Method.GET, params, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String, String> resultMap = VolleyHelper.checkResult(response);
                        if (VolleyHelper.isResultError(resultMap)) {
                            onDataResultListener.onFailure(resultMap.get(VolleyHelper.MESSAGE), new Exception());
                        } else {
                            onDataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), new Exception());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onDataResultListener.onFailure(error.getMessage(), new Exception());
                }
            });
        } else {
            TestData.getTestData(mContext, "alldata.txt", onDataResultListener);
        }
    }

    @Override
    public void searchStopsByKeywords(HashMap<String, String> params, final ImplDataResultListener onDataResultListener) {
        if (!TestData.isOpenDemoData(mContext)) {
            VolleyHelper volleyHelper = new VolleyHelper(mContext);
            volleyHelper.createRequest(RestRequestConstants.MAIN_SEARCH_STATION_BY_KEYWORDS, Request.Method.GET, params, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        HashMap<String, String> resultMap = VolleyHelper.checkResult(response);
                        if (VolleyHelper.isResultError(resultMap)) {
                            onDataResultListener.onFailure(resultMap.get(VolleyHelper.MESSAGE), new Exception());
                        } else {
                            onDataResultListener.onSuccess(resultMap.get(VolleyHelper.RESULT));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), new Exception());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onDataResultListener.onFailure(ResourcesHelper.getString(mContext, R.string.message_error_request), new Exception());
                }
            });
        }else{
            TestData.getTestData(mContext, "searchStop.txt", onDataResultListener);
        }
    }

    @Override
    public void setContext(Context context) {
        this.mContext = context;
    }
}
