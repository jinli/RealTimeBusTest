package com.ct.realtimebus;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.AndroidTestCase;
import android.util.Log;

import com.ct.realtimebus.mvp.model.ImplDataResultListener;
import com.ct.realtimebus.mvp.model.LineModel;
import com.ct.realtimebus.mvp.model.inf.ILineModel;
import com.ct.realtimebus.test.TestButtonActivity;

import junit.framework.Test;

import java.util.HashMap;

/**
 * Created by zhangjinli on 16/9/18.
 */
public class LineModelTest extends AndroidTestCase {

    public static final String TAG = "LineModeTestActivity";

    public Context context;;

    public void test_lineMode(){
        context = getContext();
        SharedPreferences sp = context.getSharedPreferences(TestButtonActivity.TEST_SETTING,0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(TestButtonActivity.DEMO_DATA,true);
        editor.commit();

        ILineModel lineModel = new LineModel();
        lineModel.setContext(context);
        lineModel.getLineInfoById(25,new HashMap<String,String>(),new ImplDataResultListener(context){
            @Override
            public void onSuccess(String resultData) {
                Log.v(TAG,resultData);
            }
        });
    }
}