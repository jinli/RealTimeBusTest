package com.ct.realtimebus.activity;

import android.content.Context;
import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import com.ct.realtimebus.main.activity.MainActivity;

/**
 * Created by zhangjinli on 16/9/18.
 */
public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

    Intent intent;
    Context context;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        intent = new Intent(getInstrumentation().getTargetContext(),MainActivity.class);
        context = getInstrumentation().getContext();
    }

    @MediumTest
    public void testStartActivity(){
        startActivity(intent, null, null);
        assertNotNull("Intent was null", intent);
    }
}
