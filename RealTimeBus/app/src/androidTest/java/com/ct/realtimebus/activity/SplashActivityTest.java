package com.ct.realtimebus.activity;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import com.ct.realtimebus.splash.activity.SplashActivity;

/**
 * Created by zhangjinli on 16/9/18.
 */
public class SplashActivityTest extends ActivityInstrumentationTestCase2<SplashActivity> {
    Intent intent;
    public SplashActivityTest() {
        super(SplashActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        intent = new Intent(getInstrumentation().getContext(),null);
        getActivity().startActivity(intent);
    }

    @MediumTest
    public void testStartActivity(){

    }

}
