package com.ct.realtimebus.main.activity;

import android.test.ActivityInstrumentationTestCase2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by zhangjinli on 16/9/18.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testOnCreate() throws Exception {

    }

    @Test
    public void testOnLocationChanged() throws Exception {

    }

    @Test
    public void testInitFragment() throws Exception {

    }

    @Test
    public void testOnCheckedChanged() throws Exception {

    }

    @Test
    public void testOnKeyDown() throws Exception {

    }

    @Test
    public void testOnRequestPermissionsResult() throws Exception {

    }

    @Test
    public void testOnStop() throws Exception {

    }

    @Test
    public void testOnDestroy() throws Exception {

    }

    @Test
    public void testOnResume() throws Exception {

    }

    @Test
    public void testOnFragmentInteraction() throws Exception {

    }

    @Test
    public void testOnLongClick() throws Exception {

    }
}